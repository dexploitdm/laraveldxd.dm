<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('image')->nullable();
            $table->string('image_two')->nullable();
            $table->string('image_three')->nullable();
            $table->string('image_blocks')->nullable();
            $table->text('content');
            $table->text('desc');
            $table->text('text_block')->nullable();
            $table->text('keywords')->nullable();
            $table->date('date')->nullable();
            $table->string('links');
            $table->string('github')->nullable();
            $table->integer('filter_id')->nullable();
            $table->string('technology')->nullable();
            $table->integer('is_slider')->default(0);
            $table->integer('blocks_image')->default(0);
            $table->string('likes')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('works');
    }
}
