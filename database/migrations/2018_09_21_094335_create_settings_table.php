<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('address')->nullable();
            $table->text('phone')->nullable();
            $table->text('time_work')->nullable();
            $table->string('logo')->nullable();
            $table->string('banner')->nullable();
            $table->string('facebook')->nullable();
            $table->string('vk')->nullable();
            $table->string('twitter')->nullable();
            $table->string('google')->nullable();
            $table->string('youtube')->nullable();
            $table->string('github')->nullable();
            $table->text('banner_text')->nullable();
            $table->string('banner_link')->nullable();
            $table->string('banner_title')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
