@if ($paginator->hasPages())
    <ul class="pagination">
        @if ($paginator->onFirstPage())
            {{--<li class="disabled"><span>&laquo;</span></li>--}}
        @else
            <a href="{{ $paginator->previousPageUrl() }}" class="page-numbers prev">
                <svg class="btn-prev">
                    <use xlink:href="#arrow-left"></use>
                </svg>
            </a>
        @endif
        @foreach ($elements as $element)
            @if (is_string($element))
                <li class="disabled bg-border-color"><span>{{ $element }}</span></li>
            @endif
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="active bg-border-color"><span>{{ $page }}</span></li>
                    @else
                        <li class="bg-border-color"><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach
        @if ($paginator->hasMorePages())
            <a href="{{ $paginator->nextPageUrl() }}" class="page-numbers next">
                <svg class="btn-next">
                    <use xlink:href="#arrow-right"></use>
                </svg>
            </a>
        @else
            {{--<li class="disabled"><span>&raquo;</span></li>--}}
        @endif
    </ul>
@endif