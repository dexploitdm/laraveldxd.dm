<div class="container-fluid bg-green-color">
    <div class="row">
        <div class="container">

            <div class="row">

                <div class="subscribe scrollme subscribeformfix">

                    <div class="col-lg-6 col-lg-offset-5 col-md-6 col-md-offset-5 col-sm-12 col-xs-12">
                        <h4 class="subscribe-title">Email Подписка!</h4>
                        <form class="subscribe-form" action="/subscribe" method="post">
                            {{csrf_field()}}
                            <input class="email input-standard-grey input-white" name="email" required="required"
                                   placeholder="Введите Вашу почту" type="email">
                            <button class="subscr-btn">подписаться
                                <span class="semicircle--right"></span>
                            </button>
                        </form>
                        <div class="sub-title">Подпишитесь на нас, чтобы быть в курсе всех событий.</div>

                    </div>

                    <div class="images-block">
                        <img src="{{asset('frontassets/img/subscr-gear.png')}}" alt="gear" class="gear">
                        <img src="{{asset('frontassets/img/subscr1.png')}}" alt="mail" class="mail">
                        <img src="{{asset('frontassets/img/subscr-mailopen.png')}}" alt="mail" class="mail-2">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>