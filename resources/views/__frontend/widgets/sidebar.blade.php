<!-- Sidebar-->
<div class="col-lg-3 col-lg-offset-1 col-md-4 col-sm-12 col-xs-12">
    <aside aria-label="sidebar" class="sidebar sidebar-right">
        <div class="widget">
            <form action="{{route('searchSimple')}}" method="GET" class="w-search">
                <input class="email search input-standard-grey" name="q" value="{{ old('q') }}" required="required"
                       placeholder="Поиск" type="search">
                <button class="icon">
                    <i class="seoicon-loupe"></i>
                </button>
            </form>
        </div>

        @if($CategoriesCloud)
        <div class="widget w-post-category">
            <div class="heading">
                <h4 class="heading-title">Категории</h4>
                <div class="heading-line">
                    <span class="short-line"></span>
                    <span class="long-line"></span>
                </div>
            </div>
            <div class="post-category-wrap">
                @foreach($CategoriesCloud as $cats)
                <div class="category-post-item">
                    <span class="post-count">{{ $cats->posts->count() }}</span>
                    <a href="#" class="category-title">{{$cats->title}}
                        <i class="seoicon-right-arrow"></i>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
        @endif
        <div class="widget w-about">
            <div class="heading">
                <h4 class="heading-title">О нас</h4>
                <div class="heading-line">
                    <span class="short-line"></span>
                    <span class="long-line"></span>
                </div>
                <p>Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit
                    litterarum formas humanitatis per seacula quarta decima quinta.
                </p>
            </div>

            <a href="02_abouts.html" class="btn btn-small btn-border c-primary">
                <span class="text">Learn More</span>
                <i class="seoicon-right-arrow"></i>
            </a>
        </div>

        <div class="widget w-request bg-boxed-red">
            <div class="w-request-content">
                <img src="{{ asset('frontassets/img/request.png') }}" alt="request">
                <h4 class="w-request-content-title">Request
                    a Free Quote</h4>
                <p class="w-request-content-text">Gothica, quam nunc putamus parum claram, anteposuerit
                    litterarum formas humanitatis.
                </p>

                <a href="22_contacts.html" class="btn btn-small btn--dark btn-hover-shadow">
                    <span class="text">Contact Now</span>
                </a>
            </div>
        </div>

        @if($recentPosts)
        <div class="widget w-latest-news">
            <div class="heading">
                <h4 class="heading-title">Последние записи</h4>
                <div class="heading-line">
                    <span class="short-line"></span>
                    <span class="long-line"></span>
                </div>
            </div>

            <div class="latest-news-wrap">
                @foreach($recentPosts as $recentPost)
                <div class="latest-news-item">
                    <div class="post-additional-info">
                        <span class="post__date">
                            <i class="seoicon-clock"></i>
                            <time class="published" datetime="2016-04-23 12:00:00">
                                {{$recentPost->getDate()}}
                            </time>
                        </span>
                    </div>
                    <h5 class="post__title entry-title ">
                        <a href="15_blog_details.html">{{$recentPost->title}}</a>
                    </h5>
                </div>
                @endforeach
            </div>

            <a href="{{route('blog.index')}}" class="btn btn-small btn--dark btn-hover-shadow">
                <span class="text">Все записи</span>
                <i class="seoicon-right-arrow"></i>
            </a>
        </div>
        @endif
        <div class="widget w-follow">
            <div class="heading">
                <h4 class="heading-title">Подписывайтесь на нас</h4>
                <div class="heading-line">
                    <span class="short-line"></span>
                    <span class="long-line"></span>
                </div>
            </div>
            <div class="w-follow-wrap">
                <div class="w-follow-item facebook-bg-color">
                    <a href="#" class="w-follow-social__item table-cell">
                        <i class="seoicon-social-facebook"></i>
                    </a>
                    <a href="#" class="w-follow-title table-cell">Facebook
                        <span class="w-follow-add">
									<i class="seoicon-cross plus"></i>
									<i class="seoicon-check-bold check"></i>
								</span>
                    </a>
                </div>
                <div class="w-follow-item twitter-bg-color">
                    <a href="#" class="w-follow-social__item table-cell">
                        <i class=" seoicon-social-twitter"></i>
                    </a>
                    <a href="#" class="w-follow-title table-cell">Twitter
                        <span class="w-follow-add active">
									<i class="seoicon-cross plus"></i>
									<i class="seoicon-check-bold check"></i>
								</span>
                    </a>
                </div>
                <div class="w-follow-item linkedin-bg-color">
                    <a href="#" class="w-follow-social__item table-cell">
                        <i class="seoicon-social-linkedin"></i>
                    </a>
                    <a href="#" class="w-follow-title table-cell">Linkedin
                        <span class="w-follow-add">
									<i class="seoicon-cross plus"></i>
									<i class="seoicon-check-bold check"></i>
								</span>
                    </a>
                </div>
                <div class="w-follow-item google-bg-color">
                    <a href="#" class="w-follow-social__item table-cell">
                        <i class="seoicon-social-google-plus"></i>
                    </a>
                    <a href="#" class="w-follow-title table-cell">Google+
                        <span class="w-follow-add">
									<i class="seoicon-cross plus"></i>
									<i class="seoicon-check-bold check"></i>
								</span>
                    </a>
                </div>
                <div class="w-follow-item pinterest-bg-color">
                    <a href="#" class="w-follow-social__item table-cell">
                        <i class="seoicon-social-pinterest"></i>
                    </a>
                    <a href="#" class="w-follow-title table-cell">Pinterest
                        <span class="w-follow-add">
									<i class="seoicon-cross plus"></i>
									<i class="seoicon-check-bold check"></i>
								</span>
                    </a>
                </div>
            </div>

        </div>

        <div class="widget w-tags">
            <div class="heading">
                <h4 class="heading-title">Теги</h4>
                <div class="heading-line">
                    <span class="short-line"></span>
                    <span class="long-line"></span>
                </div>
            </div>

            <div class="tags-wrap">
                @if($tagsCloud)
                    @foreach($tagsCloud as $tag)
                        <a href="{{route('tag.show', $tag->slug)}}" class="w-tags-item">{{$tag->title}}</a>
                    @endforeach
                @endif
            </div>
        </div>
    </aside>
</div>
<!-- End Sidebar-->