<div class="container">
    <div class="row pt100">

        <div class="col-lg-12">

            <div class="heading align-center pb80">
                <h4 class="h1 heading-title">Хотите заказать сайт?</h4>
                <div class="heading-line">
                    <span class="short-line"></span>
                    <span class="long-line"></span>
                </div>
                <p class="heading-text">Заполните форму внизу и наши специалисты свяжутся с вами!</p>
            </div>
            <img src="{{asset('frontassets/img/seo-analysis.png') }}" alt="dexploitdm">
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row bg-boxed-black medium-padding120">
        <div class="container">
            <div>
                <form class="contact-form" action="/order" method="post">
                    {{csrf_field()}}
                    <div class="col-lg-8 col-lg-offset-2 col-md-12 col-md-offset-0 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <label for="contact_website" class="input-title">Какого вида, сайт Вас интересует?<abbr class="required" title="required">*</abbr></label>
                                <span class="checked-icon">
                                <input class="email focus-white input-standard-grey input-dark" id="" name="viewsite" required="" placeholder="" type="text">
                            </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label for="contact_name" class="input-title">Ваше имя<abbr class="required" title="required">*</abbr></label>
                                <span class="checked-icon">
                                    <input class="email focus-white input-standard-grey input-dark" id="contact_name" required="" name="name" placeholder="" type="text">
                                  </span>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label for="contact_email" class="input-title">Ваш Email<abbr class="required" title="required">*</abbr></label>
                                <span class="checked-icon">
                                    <input class="email focus-white input-standard-grey input-dark" id="contact_email" required="" name="email" placeholder="" type="email">
                                </span>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                                <label for="contact_phone" class="input-title">Телефон:</label>

                                <input class="email focus-white input-standard-grey input-dark" id="contact_phone" name="phone" placeholder="" type="text">

                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                                <label for="contact_company" class="input-title">Компания:</label>

                                <input class="email focus-white input-standard-grey input-dark" id="contact_company" name="company" placeholder="" type="text">

                            </div>

                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <label for="contact_details" class="input-title">Детали:</label>
                                <textarea class="email focus-white input-standard-grey input-dark" id="contact_details" name="message" placeholder=""></textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="submit-block table">
                                <div class="col-lg-5 table-cell">
                                    <button class="btn btn-medium btn--green btn-hover-shadow">
                                        <span class="text">Отправить</span>
                                        <span class="semicircle"></span>
                                    </button>
                                </div>

                                <div class="col-lg-7 table-cell">
                                    <div class="submit-block-text">
                                        Заполните форму и наши специалисты свяжутся с Вами в течение нескольких часов.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>