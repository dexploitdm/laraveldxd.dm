@extends('frontend.layout')
@section('title')
    {{$work->title}}
@stop
@section('description')
    {{str_limit($work->content,120)}}
@stop
@section('keywords')
    {{$work->keywords}}
@stop
@section('content')
    <div class="stunning-header stunning-header-bg-lightviolet paddingfixpage">
        <div class="stunning-header-content">
            <h1 class="stunning-header-title">{{$work->title}}</h1>
            <ul class="breadcrumbs">
                <li class="breadcrumbs-item">
                    <a href="{{route('home')}}">Главная</a>
                    <i class="seoicon-right-arrow"></i>
                </li>
                <li class="breadcrumbs-item active">
                    <span href="#">{{$work->title}}</span>
                    <i class="seoicon-right-arrow"></i>
                </li>
            </ul>
        </div>
    </div>
    @if(session('status'))
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if($work->is_slider == 1)
        <div class="container-fluid" id="workshowblocks">
            <div class="row bg-border-color medium-padding30">
                <div class="container">
                    <div class="row">
                        <div class="">
                            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                <div class="swiper-container image_blocks" data-effect="fade">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <div class="product-description-ver2-thumb">
                                                <img src="{{$work->getImage()}}" alt="description" class="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content_blocks_mar col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-12 col-xs-12">
                                <div class="product-description-ver2-content">
                                    <time class="post__date published" datetime="2016-04-03 12:00:00">
                                        {{$work->getDate()}}
                                    </time>
                                    <div class="heading">
                                        <h4 class="h1 heading-title">{{$work->title}}</h4>
                                        {!! $work->desc !!}
                                    </div>
                                    <div class="likes-block">
                                        @if($work->github)
                                            <a href="{{$work->github}}" target="_blank">
                                                <div class="btn btn-medium btn--dark btn-hover-shadow">
                                                    <span class="text">Исходник<i class="fa fa-github fa-2x" aria-hidden="true"></i></span>
                                                    <span class="semicircle"></span>
                                                </div>
                                            </a>
                                        @else
                                            <a href="{{$work->links}}" target="_blank">
                                                <div class="btn btn-medium btn--dark btn-hover-shadow">
                                                    <span class="text">Посмотреть</span>
                                                    <span class="semicircle"></span>
                                                </div>
                                            </a>
                                        @endif
                                        <a href="#" name="likes" onclick="plusLikes()" class="likes">
                                            <i class="seoicon-shape-heart"></i>
                                            <span class="count-likes">{{$work->likes}}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="blockswork">
                            <div class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-12 col-xs-12">
                                <div class="product-description-content">
                                    <div class="heading contentwork">
                                        {!! $work->content !!}
                                    </div>
                                </div>
                            </div>
                            <div class="blocks_two_image col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="product-description-thumb">
                                    <img src="{{$work->getImageTwo()}}" alt="{{$work->title}}" class="shadow-image">
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($work->text_block)
                        <div class="row">
                            <div class="blockswork">
                                <div class="blocks_two_image col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="product-description-thumb">
                                        <img src="{{$work->getImageBlocks()}}" alt="{{$work->title}}" class="shadow-image">
                                    </div>
                                </div>
                                <div class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-12 col-xs-12">
                                    <div class="product-description-content">
                                        <div class="heading contentwork">
                                            {!! $work->content !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($work->blocks_image)
                        <div class="row">
                            <div class="blockswork">
                                <div class="blocks_two_image col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="product-description-thumb">
                                        <img src="{{$work->getImageThree()}}" alt="{{$work->title}}" class="shadow-image">
                                    </div>
                                </div>
                                <div class="blocks_two_image col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="product-description-thumb">
                                        <img src="{{$work->getImageBlocks()}}" alt="{{$work->title}}" class="shadow-image">
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <script>function plusLikes() {location.href = '/works/like/{{$work->id}}';}</script>
    @else
        <div class="container-fluid" id="workshowother">
            <div class="row medium-padding30">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                            <div class="heading align-center">
                                <h4 class="h1 heading-title">{{$work->title}}</h4>
                                <div class="heading-line">
                                    <span class="short-line"></span>
                                    <span class="long-line"></span>
                                </div>
                            </div>
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <img src="{{$work->getImage()}}" alt="marketing">
                            </div>
                            <div class="contentwork">
                                {!! $work->content !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @include('frontend.widgets.orderformworks')
    @include('frontend.widgets.emailsubscribe')
@endsection