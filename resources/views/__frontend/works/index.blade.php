@extends('frontend.layout')
@section('title','| Проекты')
@section('description','Мы предлагаем обширный спектр услуг')
@section('keywords','разработка сайтов, заказаь сайт, заказать интернет магазин, заказать фирменный стиль')
@section('content')
    <div class="stunning-header stunning-header-bg-lightgray paddingfixpage services_dxd">
        <div class="stunning-header-content">
            <h1 class="stunning-header-title c-dark">Проекты</h1>
            <ul class="breadcrumbs">
                <li class="breadcrumbs-item">
                    <a href="{{route('home')}}" class="c-gray">Главная</a>
                    <i class="seoicon-right-arrow"></i>
                </li>
                <li class="breadcrumbs-item active">
                    <span href="{{route('service.index')}}" class="c-primary">проекты</span>
                    <i class="seoicon-right-arrow"></i>
                </li>
            </ul>
        </div>
    </div>
    @if(session('status'))
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if($works)
        <div class="container">
            <div class="row medium-padding120">

                <div class="col-lg-12">

                    <div class="heading align-center">
                        <h4 class="h1 heading-title">Наши проекты</h4>
                    </div>
                    <div id="js-filters-masonry" class="cbp-l-filters-alignRight">
                        <div data-filter="*" class="cbp-filter-item-active cbp-filter-item">
                            Все
                        </div>
                        @foreach($filters as $filter)
                        <div data-filter=".{{$filter->slug}}" class="cbp-filter-item">
                            {{$filter->title}}
                        </div>
                        @endforeach
                    </div>
                    <div id="js-grid-masonry" class="cbp">
                        @foreach($works as $work)
                        <div class="cbp-item {{$work->filter->slug}}">
                            <a href="{{route('work.show', $work->slug)}}" class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img src="{{$work->getImage()}}" alt="{{$work->title}}">
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <div class="cbp-l-caption-title">{{$work->title}}</div>
                                            <div class="cbp-l-caption-desc">{{$work->filter->title}}</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif



    @include('frontend.widgets.emailsubscribe')
@endsection
