@extends('frontend.layout')
@section('content')
    <div class="stunning-header stunning-header-bg-lightviolet paddingfixpage">
        <div class="stunning-header-content">
            <h1 class="stunning-header-title">Категория: {{$category->title}}</h1>
            <ul class="breadcrumbs">
                <li class="breadcrumbs-item">
                    <a href="{{route('home')}}">Главная</a>
                    <i class="seoicon-right-arrow"></i>
                </li>
                <li class="breadcrumbs-item active">
                    <span href="/blogs">Статьи</span>
                    <i class="seoicon-right-arrow"></i>
                </li>
            </ul>
        </div>
    </div>
    @if(session('status'))
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="container">
        <div class="row medium-padding120">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                @if($posts)
                    <main class="main">
                        @foreach($posts as $post)
                            <article class="hentry post post-standard has-post-thumbnail sticky">

                                <div class="post-thumb">
                                    <img src="{{$post->getImage()}}" alt="{{$post->title}}">
                                    <div class="overlay"></div>
                                    <a href="{{$post->getImage()}}" class="link-image js-zoom-image">
                                        <i class="seoicon-zoom"></i>
                                    </a>
                                    <a href="{{route('blog.show', $post->slug)}}" class="link-post">
                                        <i class="seoicon-link-bold"></i>
                                    </a>
                                </div>
                                <div class="post__content">
                                    <div class="post__author author vcard">
                                        <img src="{{ asset('uploads/'.$post->author->avatar ) }}" alt="{{$post->author->name}}">
                                        Автор
                                        <div class="post__author-name fn">
                                            <a class="post__author-link">{{$post->author->name}}</a>
                                        </div>
                                    </div>

                                    <div class="post__content-info">
                                        <h2 class="post__title entry-title ">
                                            <a href="{{route('blog.show', $post->slug)}}">{{$post->title}}</a>
                                        </h2>
                                        <div class="post-additional-info">
									<span class="post__date">
										<i class="seoicon-clock"></i>
										<time class="published" datetime="2016-04-17 12:00:00">
											{{$post->getDate()}}
										</time>
									</span>
                                            @if($post->hasCategory())
                                                <span class="category">
										<i class="seoicon-tags"></i>
										<a href="{{route('category.show', $post->category->slug)}}">{{$post->getCategoryTitle()}}</a>
									</span>
                                            @endif
                                            <span class="post__comments">
										<a href="#"><i class="fa fa-comment-o" aria-hidden="true"></i></a>
										6
									</span>
                                        </div>

                                        <p class="post__text">{!! str_limit($post->content, 100)  !!}</p>

                                        <a href="{{route('blog.show', $post->slug)}}" class="btn btn-small btn--dark btn-hover-shadow">
                                            <span class="text">Читать</span>
                                            <i class="seoicon-right-arrow"></i>
                                        </a>
                                    </div>
                                </div>

                            </article>
                        @endforeach
                    </main>
                @endif
                <div class="row">
                    <div class="col-lg-12">
                        <nav class="navigation">
                            {{$posts->links('frontend.widgets.paginate')}}
                        </nav>
                    </div>
                </div>
            </div>
            @include('frontend.widgets.sidebar')
        </div>
    </div>
    @include('frontend.widgets.sidebar')
@endsection
