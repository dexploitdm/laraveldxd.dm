@extends('frontend.layout')
@section('title')
    {{$post->title}}
@stop
@section('description')
    {{str_limit($post->content,120)}}
@stop
@section('keywords')
    {{$post->keywords}}
@stop
@section('content')
    <div class="stunning-header stunning-header-bg-lightviolet paddingfixpage">
        <div class="stunning-header-content">
            <h1 class="stunning-header-title">{{$post->title}}</h1>
            <ul class="breadcrumbs">
                <li class="breadcrumbs-item">
                    <a href="{{route('home')}}">Главная</a>
                    <i class="seoicon-right-arrow"></i>
                </li>
                <li class="breadcrumbs-item active">
                    <span href="#">{{$post->title}}</span>
                    <i class="seoicon-right-arrow"></i>
                </li>
            </ul>
        </div>
    </div>
    @if(session('status'))
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="container">
        <div class="row medium-padding120">
            <main class="main">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <article class="hentry post post-standard-details">
                        <div class="post-thumb">
                            <img src="{{$post->getImage()}}" alt="{{$post->title}}">
                        </div>

                        <div class="post__content">
                            <h2 class="h2 post__title entry-title ">
                                <a href="{{route('blog.show', $post->slug)}}">{{$post->title}}t</a>
                            </h2>

                            <div class="post-additional-info">
                                <div class="post__author author vcard">
                                    <img src="{{asset('/uploads/'. $post->author->avatar)}}" alt="{{$post->author->name}}">
                                    Автор
                                    <div class="post__author-name fn">
                                        <a class="post__author-link">{{$post->author->name}}</a>
                                    </div>
                                </div>
                                <span class="post__date">
								<i class="seoicon-clock"></i>
								<time class="published" datetime="2016-03-20 12:00:00">
									{{$post->getDate()}}
								</time>
							</span>
                            <span class="category">
								<i class="seoicon-tags"></i>
								<a href="{{route('category.show', $post->category->slug)}}">{{$post->getCategoryTitle()}}</a>
							</span>
                            <span class="post__comments">
								<a href="#"><i class="fa fa-comment-o" aria-hidden="true"></i></a>
								6
							</span>
                            </div>
                            <div class="post__content-info">
                                {!! $post->content !!}
                                <div class="widget w-tags">
                                    <div class="tags-wrap">
                                        @foreach($post->tags as $tag)
                                            <a href="{{route('tag.show', $tag->slug)}}" class="w-tags-item">{{$tag->title}}</a>
                                        @endforeach
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="socials">Share:
                            <a href="" class="social__item">
                                <i class="seoicon-social-facebook"></i>
                            </a>
                            <a href="" class="social__item">
                                <i class="seoicon-social-twitter"></i>
                            </a>
                            <a href="" class="social__item">
                                <i class="seoicon-social-linkedin"></i>
                            </a>
                            <a href="" class="social__item">
                                <i class="seoicon-social-google-plus"></i>
                            </a>
                            <a href="" class="social__item">
                                <i class="seoicon-social-pinterest"></i>
                            </a>
                        </div>
                    </article>
                    <!-- Коментарий автора -->
                    <div class="blog-details-author">
                        <div class="blog-details-author-thumb">
                            <img src="{{ asset('frontassets/img/blog-details-author.png') }}" alt="Author">
                        </div>
                        <div class="blog-details-author-content">
                            <div class="author-info">
                                <h5 class="author-name">Philip Demarco</h5>
                                <p class="author-info">SEO Specialist</p>
                            </div>
                            <p class="text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
                                nonummy nibh euismod.
                            </p>
                            <div class="socials">
                                <a href="" class="social__item">
                                    <img src="{{ asset('frontassets/svg/circle-facebook.svg') }}" alt="facebook">
                                </a>
                                <a href="" class="social__item">
                                    <img src="{{ asset('frontassets/svg/twitter.svg') }}" alt="twitter">
                                </a>

                                <a href="" class="social__item">
                                    <img src="{{ asset('frontassets/svg/google.svg') }}" alt="google">
                                </a>
                                <a href="" class="social__item">
                                    <img src="{{ asset('frontassets/svg/youtube.svg') }}" alt="youtube">
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- Конец Коментарий автора -->
                    <div class="pagination-arrow">
                        @if($post->hasPrevious())
                        <a href="{{route('blog.show', $post->getPrevious()->slug)}}" class="btn-prev-wrap">
                            <svg class="btn-prev">
                                <use xlink:href="#arrow-left"></use>
                            </svg>
                            <div class="btn-content">
                                <div class="btn-content-title">Предыдущая запись</div>
                                <p class="btn-content-subtitle">{!! str_limit($post->getPrevious()->title, 30) !!}</p>
                            </div>
                        </a>
                        @endif
                        @if($post->hasNext())
                        <a href="{{route('blog.show', $post->getNext()->slug)}}" class="btn-next-wrap">
                            <div class="btn-content">
                                <div class="btn-content-title">Следущая запись</div>
                                <p class="btn-content-subtitle">{!! str_limit($post->getNext()->title, 30) !!}</p>
                            </div>
                            <svg class="btn-next">
                                <use xlink:href="#arrow-right"></use>
                            </svg>
                        </a>
                        @endif
                    </div>


                    <div class="comments">
                        <div class="heading">
                            <h4 class="h1 heading-title">Комментариев: {{$post->getComments()->count()}} </h4>
                            <div class="heading-line">
                                <span class="short-line"></span>
                                <span class="long-line"></span>
                            </div>
                        </div>
                        <ol class="comments__list">

                            @if(!$post->comments->isEmpty())
                            @foreach($post->getComments() as $comment)
                            <li class="comments__item">
                                <div class="comment-entry comment comments__article">
                                    <div class="comment-content comment">
                                        <p>{{$comment->text}}</p>
                                    </div>
                                    <div class="comments__body display-flex">
                                        <a href="#" class="reply">
                                            <i class=" seoicon-arrow-back"></i>
                                        </a>
                                        <div class="comments__avatar">
                                            <img src="{{$comment->author->getImage()}}" alt="avatar">
                                        </div>
                                        <header class="comment-meta comments__header">
                                            <cite class="fn url comments__author">
                                                <a rel="external" class=" ">{{$comment->author->name}}</a>
                                            </cite>
                                            <div class="comments__time">
                                                <time class="published" datetime="2016-04-20 12:00:00">{{$comment->created_at->diffForHumans()}}
                                                    <span class="at">at</span> 4:27 pm
                                                </time>
                                            </div>
                                        </header>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                            @endif
                        </ol>
                    </div>
                    @if(Auth::check())
                    <div class="row">
                        <div class="leave-reply contact-form">
                            <form role="form" method="post" action="/comment">
                                {{csrf_field()}}
                                <div class="col-lg-12">
                                    <div class="heading">
                                        <h4 class="h1 heading-title">Ваш комментарий</h4>
                                        <div class="heading-line">
                                            <span class="short-line"></span>
                                            <span class="long-line"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                   <input type="hidden" class="email input-standard-grey" name="post_id" value="{{$post->id}}">
                                </div>

                                <div class="col-lg-12">
                                    <textarea name="message" class="input-text input-standard-grey" placeholder="Ваше сообщение"></textarea>
                                </div>
                                <button class="btn send-btn">Post Comment</button>
                            </form>
                        </div>
                    </div>
                    @endif


                </div>
                @include('frontend.widgets.sidebar')
            </main>
        </div>
    </div>
    @include('frontend.widgets.emailsubscribe')
@endsection