@extends('frontend.layout')
@section('title','| Услуги')
@section('description','Мы предлагаем обширный спектр услуг')
@section('keywords','разработка сайтов, заказаь сайт, заказать интернет магазин, заказать фирменный стиль')
@section('content')
    <div class="stunning-header stunning-header-bg-lightgray paddingfixpage services_dxd">
        <div class="stunning-header-content">
            <h1 class="stunning-header-title c-dark">Услуги</h1>
            <ul class="breadcrumbs">
                <li class="breadcrumbs-item">
                    <a href="{{route('home')}}" class="c-gray">Главная</a>
                    <i class="seoicon-right-arrow"></i>
                </li>
                <li class="breadcrumbs-item active">
                    <span href="{{route('service.index')}}" class="c-primary">Услуги</span>
                    <i class="seoicon-right-arrow"></i>
                </li>
            </ul>
        </div>
    </div>
    @if(session('status'))
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if($services)
        <div class="container">
            <div class="row pt30 pb30">
                <div class="col-lg-12">
                    <div class="heading">
                        <p class="heading-text c-dark dxd_center">Мы предоставляем следующий спектр услуг</p>
                    </div>
                </div>

                @foreach($services as $service)
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="servises-item bg-violet-color" style="background-color: {{$service->background}};">
                            <div class="servises-item__thumb">
                                <img src="{{$service->getImage()}}" alt="service">
                            </div>
                            <div class="servises-item__content">
                                <h4 class="servises-title">{{$service->title}}</h4>
                                <p class="servises-text">{!! $service->desc !!}</p>
                            </div>

                            <a href="{{route('service.show', $service->slug)}}" class="read-more"><i class="seoicon-right-arrow"></i></a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
    <div class="container-fluid">
        <div class="row pt80 pb80 bg-boxed-blue">
            <div class="container">
                <div class="row">

                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 table-cell">

                        <div class="heading">
                            <h4 class="h1 heading-title c-white no-margin">Хотите воспользоваться нашими услугами?</h4>
                            <p class="heading-text c-white no-margin">
                                Заполните форму обратной связи с сообщением о заказе и отправьте.
                                После чего я свяжусь с вами по указанным контактам
                                в форме и мы обсудим детально тех. задание, сроки, так же отвечу на все интересующие Вас вопросы.
                            </p>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 table-cell">
                        <a href="#" class="btn btn-medium btn--dark btn-hover-shadow">
                            <span class="text">Заполнить</span>
                            <span class="semicircle"></span>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @include('frontend.widgets.emailsubscribe')
@endsection
