@extends('frontend.layout')
@section('title')
    | {{$service->title}}
@stop
@section('description')
    {{str_limit($service->desc,120)}}
@stop
@section('keywords')
    {{$service->keywords}}
@stop
@section('content')
    <div class="stunning-header stunning-header-bg-lightgray paddingfixpage services_dxd nohonehead">
        <div class="stunning-header-content dxdulheading">
            <ul class="breadcrumbs">
                <li class="breadcrumbs-item">
                    <a href="{{route('home')}}" class="c-gray">Главная</a>
                    <i class="seoicon-right-arrow"></i>
                </li>
                <li class="breadcrumbs-item">
                    <a href="{{route('service.index')}}" class="c-gray">Услуги</a>
                    <i class="seoicon-right-arrow"></i>
                </li>
                <li class="breadcrumbs-item active">
                    <span href="#" class="c-primary">{{$service->title}}</span>
                    <i class="seoicon-right-arrow"></i>
                </li>
            </ul>
        </div>
    </div>
    @if(session('status'))
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="container">
        <div class="row pt80">
            <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                <div class="heading align-center mb60 dxdcontent post__content-info">
                    <h1 class="h1 heading-title">{{$service->title}}</h1>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>
                    {!! $service->content !!}
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="seo-score scrollme">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-xs-12 col-sm-12">
                            <div class="seo-score-content align-center">
                                <div class="heading align-center">
                                    <h4 class="h1 heading-title">Хотите заказать услугу?</h4>
                                    <p class="heading-text">Напишите нам!</p>
                                </div>
                                <div class="seo-score-form">
                                    <form class="seo-score-form input-inline" method="post" action="send_mail.php">
                                        <div class="row">
                                            <div class="col-lg-8 no-padding col-md-12 col-xs-12 col-sm-12">
                                                <input name="permalink" class="input-dark site" required="required" placeholder="Что Вас интересует?">
                                            </div>
                                            <div class="col-lg-4 no-padding col-md-12 col-xs-12 col-sm-12">
                                                <input name="email" class="input-dark e-mail" required="required" placeholder="Ваш Email" type="email">
                                            </div>
                                        </div>
                                        <button class="btn btn-medium btn--green btn-hover-shadow">
                                            <span class="text">Отправить!</span>
                                            <span class="semicircle"></span>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="images">
                    <img src="{{asset('frontassets/img/seoscore1.png') }}" alt="image" class="" style="opacity: 1; top: -10px;">
                    <img src="{{asset('frontassets/img/seoscore2.png') }}" alt="image" class="" style="opacity: 1; bottom: 0px;">
                    <img src="{{asset('frontassets/img/seoscore3.png') }}" alt="image" class="" style="opacity: 1; bottom: 0px;">
                </div>

            </div>
        </div>
    </div>
    @include('frontend.widgets.emailsubscribe')
@endsection