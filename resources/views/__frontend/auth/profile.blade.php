@extends('frontend.layout')
@section('content')
    <div class="container">
        <div class="contact-form medium-padding120">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="heading">
                        <h4 class="heading-title">Профиль</h4>
                        <div class="heading-line">
                            <span class="short-line"></span>
                            <span class="long-line"></span>
                        </div>
                        @if(session('status'))
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="alert alert-success">
                                            {{session('status')}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @include('backend.message.errors')
                        <p class="heading-text dxdp">Please contact us using the form and we’ll get back to you as soon as possible.</p>
                        <a href="{{route('logout')}}">
                            <div class="btn btn-medium btn--dark dxdfloat" style="margin-bottom: 20px; margin-left: 0;">
                                <span class="text">Выход</span>
                                <span class="semicircle"></span>
                                <span class="semicircle"></span>
                            </div>
                        </a>
                        @if($user->is_admin == '1')
                            <a href="{{route('backend')}}">
                                <div class="btn btn-small btn--primary" style="margin-bottom: 20px; margin-left: 0;">
                                    <span class="text">Панель администратора</span>
                                    <i class="seoicon-right-arrow"></i>
                                </div>
                            </a>
                        @endif
                    </div>
                </div>
                <div class="col-lg-6 mb30">
                    <div class="testimonial-item testimonial-arrow">
                        <div class="testimonial-text">
                            Investigationes demonstraverunt lectores legere me lius quod ii legunt.
                        </div>
                        <div class="author-info-wrap table">
                            <div class="testimonial-img-author round authorcustom_avatar">
                            @if($user->getImage())
                                <img src="{{$user->getImage()}}" alt="author">
                            @endif
                            </div>
                            <div class="author-info table-cell">
                                <h6 class="author-name">{{$user->name}}</h6>
                                <div class="author-company c-primary">Envato Market</div>
                            </div>
                        </div>
                        <div class="quote">
                            <i class="seoicon-quotes"></i>
                        </div>
                    </div>

                </div>
            </div>

            <form class="contact-form loginform" method="post" action="/profile" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <input name="name" class="email input-standard-grey" placeholder="Ваше Имя" value="{{$user->name}}" type="text" required="">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <input name="email" class="email input-standard-grey" placeholder="Ваш email" value="{{$user->email}}" type="email" required="">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <input name="password" class="email input-standard-grey" placeholder="Ваш пароль" type="text" required="">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <input name="credo" class="credo input-standard-grey" placeholder="Ваша должность" value="{{$user->credo}}" type="text">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <textarea name="message" class="email input-standard-grey" placeholder="Расскажите о себе"></textarea>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 btnupload">
                        <input type="file" class="btnhodden btn btn-small btn-border c-primary" id="image" name="avatar">
                        <div class="bt_image btn btn-small btn-border c-primary" style="margin-bottom: 20px; margin-left: 0;">
                            <span class="text">Загрузить фото</span>
                        </div>
                        <button  type="submit"  class="btn btn-medium btn--secondary">
                            <span class="text">Сохранить профиль</span>
                        </button>
                        <div class="submit-block-text textprofile">
                            Please, let us know any particular things to check and the best time
                            to contact you by phone (if provided).
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

