@extends('frontend.layout')
@section('title','| О нас')
@section('description','Мы предлагаем обширный спектр услуг')
@section('keywords','разработка сайтов, заказаь сайт, заказать интернет магазин, заказать фирменный стиль')
@section('content')
    <div class="stunning-header stunning-header-bg-lightgray paddingfixpage services_dxd">
        <div class="stunning-header-content">
            <h1 class="stunning-header-title c-dark">О нас</h1>
            <ul class="breadcrumbs">
                <li class="breadcrumbs-item">
                    <a href="{{route('home')}}" class="c-gray">Главная</a>
                    <i class="seoicon-right-arrow"></i>
                </li>
                <li class="breadcrumbs-item active">
                    <span href="{{route('about')}}" class="c-primary">О нас</span>
                    <i class="seoicon-right-arrow"></i>
                </li>
            </ul>
        </div>
    </div>
    @if(session('status'))
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if($abouts_one)
    <div class="container">
        <div class="row pt30">
            @foreach($abouts_one as $abouts_on)
            <div class="col-lg-12">
                <div class="heading mb30">
                    <h4 class="h1 heading-title">{{$abouts_on->title}}</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>
                    <h5 class="heading-subtitle">{!! $abouts_on->content !!}
                    </h5>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @endif
    @if($absliders)
    <div class="section bg-greendark-color">
        <div class="container">
            <div class="slider-profit-wrap">
                <div class="swiper-container auto-height pagination-vertical swiper-swiper-unique-id-0 initialized swiper-container-vertical swiper-container-autoheight" data-direction="vertical" data-loop="false" data-mouse-scroll="true" id="swiper-unique-id-0">
                    <div class="swiper-wrapper" style="height: 521px; transform: translate3d(0px, -521px, 0px); transition-duration: 0ms;">
                    @foreach($absliders as $abslider)
                        <div class="slider-profit swiper-slide" style="height: 521px;">
                            <div class="row medium-padding120">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="heading">
                                        <h4 class="h1 heading-title c-white mb30">{{$abslider->title}}</h4>
                                        <div class="c-white">{!! $abslider->content !!}
                                        </div>
                                    </div>
                                    @if($abslider->btn_name)
                                    <a href="{{$abslider->btn_link}}" class="btn btn-medium btn--dark btn-hover-shadow">
                                        <span class="text">{{$abslider->btn_name}}</span>
                                        <span class="semicircle"></span>
                                    </a>
                                    @endif
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="slider-profit-thumb">
                                        <img src="{{$abslider->getImage()}}" alt="profit">
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                    <div class="swiper-pagination pagination-swiper-unique-id-0 swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet"></span><span class="swiper-pagination-bullet swiper-pagination-bullet-active"></span><span class="swiper-pagination-bullet"></span><span class="swiper-pagination-bullet"></span></div>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if($commands)
    <div class="container">
        <div class="row pt120 mb30">
            <div class="col-lg-12">
                <div class="heading align-center">
                    <h4 class="h1 heading-title">Наша команда</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>
                    <p class="heading-text">Специалисты</p>
                </div>
            </div>

        </div>
        <?php $countcom = $commands->count();
        if($countcom == 3): $i = 4;
        elseif($countcom == 2): $i = 6;
        elseif($countcom == 4): $i = 3;
        endif; ?>
        <div class="row pb120">
            @foreach($commands as $command)
            <div class="col-lg-<?php echo $i; ?> col-md-<?php echo $i; ?> col-sm-6 col-xs-12">
                <div class="teammembers-item">
                    <img src="{{$command->getImage()}}" alt="team member">
                    <h5 class="teammembers-item-name">{{$command->title}}</h5>
                    <p class="teammembers-item-prof">{{$command->work}}</p>

                    <div class="socials">
                        @if($command->facebook)
                        <a href="{{$command->facebook}}" class="social__item">
                            <img src="{{asset('frontassets/svg/circle-facebook.svg') }}" alt="facebook">
                        </a>
                        @endif
                        @if($command->vk)
                        <a href="{{$command->vk}}" class="social__item">
                            <img src="{{asset('frontassets/svg/vk.svg') }}" alt="vk">
                        </a>
                        @endif
                        @if($command->twitter)
                        <a href="{{$command->twitter}}" class="social__item">
                            <img src="{{asset('frontassets/svg/twitter.svg') }}" alt="twitter">
                        </a>
                        @endif
                        @if($command->google)
                        <a href="{{$command->google}}" class="social__item">
                            <img src="{{asset('frontassets/svg/google.svg') }}" alt="google">
                        </a>
                        @endif
                        @if($command->youtube)
                        <a href="{{$command->youtube}}" class="social__item">
                            <img src="{{asset('frontassets/svg/youtube.svg') }}" alt="youtube">
                        </a>
                        @endif
                        @if($command->github)
                        <a href="{{$command->github}}" class="social__item">
                            <img src="{{asset('frontassets/svg/github.svg') }}" alt="github">
                        </a>
                         @endif
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @endif
    @if($abouts_two)
    <div class="container-fluid">
        <div class="row bg-orangedark-color">
            <div class="our-vision scrollme">
                <div class="container">
                    <div class="row">
                        @foreach($abouts_two as $item)
                        <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                            <div class="heading align-center">
                                <h4 class="h1 heading-title">{{$item->title}}</h4>
                                <div class="heading-line">
                                    <span class="short-line bg-yellow-color"></span>
                                    <span class="long-line bg-yellow-color"></span>
                                </div>
                                <div class="heading-text c-white">{!! $item->content !!}
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <img src="{{asset('frontassets/img/elements.png') }}" class="elements" alt="elements" style="opacity: 1;">
                <img src="{{asset('frontassets/img/eye.png') }}" class="eye" alt="eye" style="opacity: 1; bottom: -90px;">
            </div>
        </div>
    </div>
    @endif
    @if($frameworks)
    <div class="container">
        <div class="row medium-padding120">
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="heading">
                    <h4 class="h1 heading-title">Инструменты</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>
                    <p class="heading-text">Мы разрабатываем и поддерживаем на следующих frameworks и cms
                    </p>
                </div>
                <a href="{{route('work.index')}}" class="btn btn-medium btn--dark mb30">
                    <span class="text">Посмотреть проекты</span>
                    <span class="semicircle"></span>
                </a>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="clients-item-wrap">
                        @foreach($frameworks as $framework)
                        <div class="client-item mb60">
                            <a class="client-image">
                                <img src="{{$framework->getImage()}}" alt="{{$framework->title}}" class="hover">
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @include('frontend.widgets.emailsubscribe')
@endsection
