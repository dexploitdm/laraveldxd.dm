@foreach($items as $item)
    <li {{ (URL::current() == $item->url()) ? "class=active" : '' }}>
        <a href="{{ $item->url() }}">{{ $item->title }}</a>
        @if($item->hasChildren())
            <ul class="dropdown">
                @include('frontend.menu.customMenuItemsChild', ['items'=>$item->children()])
            </ul>
        @endif
@endforeach