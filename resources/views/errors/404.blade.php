@extends('frontend.layout')
@section('content')
    <div class="content-page-404">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <h4 class="title">404</h4>
                    <div class="subtitle">Sorry! Страница не найдена (</div>
                    <p class="text">Возможно, данная страница была удалена или Вы набрали неправильный адрес.</p>
                    <a href="{{route('home')}}" class="btn btn-small btn--primary btn-hover-shadow">
                        <span class="text">Вернутся на главную</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
