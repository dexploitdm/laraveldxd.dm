@extends('backend.layout')

@section('content')
    <div class="page has-sidebar-left">
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-package"></i>
                            Теги
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <ul class="nav responsive-tab nav-material nav-material-white">
                        <li>
                            <a class="nav-link active" href="panel-page-products.html"><i class="icon icon-list"></i>Все</a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{route('tags.create')}}"><i
                                        class="icon icon-plus-circle"></i> Добавить тег</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        @include('backend.message.msg')
        <div class="container-fluid animatedParent animateOnce my-3">
            <div class="animated fadeInUpShort">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card no-b shadow">
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    <table class="table table-hover ">
                                        <tbody>
                                        @if($tags)
                                            @foreach($tags as $tag)
                                                <tr class="no-b">
                                                    <td>
                                                        <h6><a href="{{route('tags.edit', $tag->id)}}">{{ $tag->title }}</a></h6>
                                                    </td>
                                                    <td>
                                                        <a href="{{route('tags.edit', $tag->id)}}" class="btn-fab btn-fab-sm btn-primary shadow text-white"><i class="icon-pencil"></i></a>
                                                        {{Form::open(['route'=>['tags.destroy', $tag->id], 'method'=>'delete'])}}
                                                        <button type="submit" class="btn btn-danger btn-xs"><i class="icon-remove"></i></button>
                                                        {{Form::close()}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <nav class="pt-3" aria-label="Page navigation">
                    {{$tags->links()}}
                </nav>
            </div>
        </div>
    </div>
@endsection