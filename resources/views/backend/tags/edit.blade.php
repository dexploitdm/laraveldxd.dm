@extends('backend.layout')

@section('content')
    <div class="page has-sidebar-left">
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-package"></i>
                            Редактирование тега
                        </h4>
                    </div>
                </div>
                @include('backend.message.errors')
                <div class="row">
                    <ul class="nav responsive-tab nav-material nav-material-white">
                        <li>
                            <a class="nav-link" href="{{route('tags.index')}}"><i class="icon icon-list"></i>Все теги</a>
                        </li>
                        <li>
                            <a class="nav-link active" href="#"><i
                                        class="icon icon-plus-circle"></i> Редактирование тега</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        <div class="container-fluid animatedParent animateOnce my-3">
            <div class="animated fadeInUpShort">
                {!! Form::open(['route'=>['tags.update',$tag->id],'method'=>'put']) !!}
                {!! csrf_field() !!}
                <div class="row">
                    <div class="col-md-8 ">
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="validationCustom01">Тег</label>
                                <input type="text" name="title" class="form-control" id="validationCustom01"
                                       placeholder="Название" value="{{$tag->title}}" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card mt-4">
                            <h6 class="card-header white">Опубликовать</h6>
                            <div class="card-footer bg-transparent">
                                <button class="btn btn-primary" type="submit">Сохранить</button>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection