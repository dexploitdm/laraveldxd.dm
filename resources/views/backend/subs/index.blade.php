@extends('backend.layout')

@section('content')
    <div class="page has-sidebar-left">
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-package"></i>
                            Подписчики
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <ul class="nav responsive-tab nav-material nav-material-white">
                        <li>
                            <a class="nav-link active" href="{{route('subscribers.index')}}"><i class="icon icon-list"></i>Все</a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{route('subscribers.create')}}"><i
                                        class="icon icon-plus-circle"></i> Добавить подписчика</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        @include('backend.message.msg')
        <div class="container-fluid animatedParent animateOnce my-3">
            <div class="animated fadeInUpShort">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card no-b shadow">
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    @if($subs)
                                    <table class="table table-hover ">
                                        <tbody>
                                        @foreach($subs as $sub)
                                            <tr class="no-b">
                                                <td>
                                                    <small class="text-muted">{{$sub->id}}</small>
                                                </td>
                                                <td><p>{{$sub->email}}</p></td>

                                                <td>
                                                    <span><i class="icon icon-timer"></i> {{$sub->created_at}}</span>
                                                </td>
                                                <td>
                                                    {{Form::open(['route'=>['subscribers.destroy', $sub->id], 'method'=>'delete'])}}
                                                    <button type="submit" class="btn btn-danger btn-xs"><i class="icon-remove"></i></button>
                                                    {{Form::close()}}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <nav class="pt-3" aria-label="Page navigation">
                    {{--{{$comments->links()}}--}}
                </nav>
            </div>
        </div>
    </div>
@endsection