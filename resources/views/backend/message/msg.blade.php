@if(Session::has('message'))
    <script>
        setTimeout(function() {
            $('#toast-container').fadeOut('fast');
        }, 3000);
    </script>
    <div id="toast-container" class="toast-bottom-left">
        <div class="toast toast-success" aria-live="polite" style="">
            <div class="toast-title">Успех!</div>
            <div class="toast-message">{{ Session::get('message') }}</div>
        </div>
    </div>
@endif