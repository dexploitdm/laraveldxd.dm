@if($errors->any())
    <script>
        setTimeout(function() {
            $('.closetoads').fadeOut('fast');
        }, 5000);
    </script>
    <div id="toast-container" class="toast-bottom-left closetoads">
    @foreach($errors->all() as $error)
            <div class="toast toast-error" aria-live="assertive" style="">
                <div class="toast-title">Ошибка!</div>
                <div class="toast-message">{{ $error }}</div>
            </div>
    @endforeach
    </div>
@endif