@extends('backend.layout')

@section('content')
    <div class="page has-sidebar-left">
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-package"></i>
                            Добавление меню
                        </h4>
                    </div>
                </div>
                @include('backend.message.errors')
                <div class="row">
                    <ul class="nav responsive-tab nav-material nav-material-white">
                        <li>
                            <a class="nav-link" href="{{route('menus.index')}}"><i class="icon icon-list"></i>Все меню навигации</a>
                        </li>
                        <li>
                            <a class="nav-link active" href="#"><i
                                        class="icon icon-plus-circle"></i> Добавление меню</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        <div class="container-fluid animatedParent animateOnce my-3">
            <div class="animated fadeInUpShort">
                    {!! Form::open(['route'=>'menus.store']) !!}
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-md-8 ">
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Название меню</label>
                                    {!! Form::text('title',isset($menu->title) ? $menu->title  : old('title'), ['class'=>'form-control','style'=>'width: auto']) !!}
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Раздел меню</label>
                                    {!! Form::select('parent', $menus, isset($menu->parent) ? $menu->parent : null, ['class'=>'form-control','style'=>'width: auto']) !!}
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Просто ссылка</label>
                                    {!! Form::radio('type', 'customLink',(isset($type) && $type == 'customLink') ? TRUE : FALSE,['class' => 'radioMenu']) !!}
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Путь для этой ссылки</label>
                                    {!! Form::text('custom_link',(isset($menu->path) && $type=='customLink') ? $menu->path  : old('custom_link'), ['placeholder'=>'Введите название ссылки','class'=>'form-control','style'=>'width: auto']) !!}
                                </div>
                                <hr>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Ссылка на статью</label>
                                    {!! Form::radio('type', 'blogLink',(isset($type) && $type == 'blogLink') ? TRUE : FALSE,['class' => 'radioMenu']) !!}
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Ссылка на категорию</label>
                                    {!! Form::select('category_alias',$categories,(isset($option) && $option) ? $option :FALSE, ['class'=>'form-control']) !!}
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Ссылка на статью</label>
                                    {!! Form::select('blog_alias', $blogs, (isset($option) && $option) ? $option :FALSE, ['placeholder' => 'Не используется','class'=>'form-control']) !!}
                                </div>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card mt-4">
                                <h6 class="card-header white">Опубликовать</h6>
                                <div class="card-footer bg-transparent">
                                    <button class="btn btn-primary" type="submit">Сохранить</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection