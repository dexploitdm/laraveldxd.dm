@extends('backend.layout')

@section('content')
    <div class="page has-sidebar-left">
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-package"></i>
                            Комментарии
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <ul class="nav responsive-tab nav-material nav-material-white">
                        <li>
                            <a class="nav-link active" href="{{route('posts.index')}}"><i class="icon icon-list"></i>Все</a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{route('posts.create')}}"><i
                                        class="icon icon-plus-circle"></i> Добавить статью</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        @include('backend.message.msg')
        <div class="container-fluid animatedParent animateOnce my-3">
            <div class="animated fadeInUpShort">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card no-b shadow">
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    @if($comments)
                                    <table class="table table-hover ">
                                        <tbody>
                                        @foreach($comments as $comment)
                                            <tr class="no-b">
                                                <td>
                                                    <h6>{{$comment->text}}</h6>
                                                    <small class="text-muted">{{$comment->id}}</small>
                                                </td>

                                                    <td>
                                                        <div class="material-switch float-right">
                                                            <input id="someSwitchOptionDefault{{$comment->id}}"
                                                                   @if($comment->status == 1)checked="checked" @endif
                                                                   name="status" type="checkbox" value="{{$comment->status}}">
                                                            <label for="someSwitchOptionDefault{{$comment->id}}" class="bg-secondary"></label>
                                                        </div>
                                                    </td>
                                                <script>
                                                    $(document).ready(function(){
                                                        $(function(){
                                                            $('#someSwitchOptionDefault{{$comment->id}}').on('change', function(){
                                                                if($('#someSwitchOptionDefault{{$comment->id}}').prop('checked')){
                                                                    location.href = '/backend/comments/toggle/{{$comment->id}}';
                                                                }else{
                                                                    location.href = '/backend/comments/toggle/{{$comment->id}}';
                                                                }
                                                            });
                                                        });
                                                    });
                                                </script>
                                                <td>
                                                    <span><i class="icon icon-timer"></i> {{$comment->created_at}}</span>
                                                </td>
                                                <td>
                                                    {{Form::open(['route'=>['comments.destroy', $comment->id], 'method'=>'delete'])}}
                                                    <button type="submit" class="btn btn-danger btn-xs"><i class="icon-remove"></i></button>
                                                    {{Form::close()}}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <nav class="pt-3" aria-label="Page navigation">
                    {{--{{$comments->links()}}--}}
                </nav>
            </div>
        </div>
    </div>
@endsection