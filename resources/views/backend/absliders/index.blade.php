@extends('backend.layout')

@section('content')
    <div class="page has-sidebar-left">
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-package"></i>
                            Слайдеры на странице о нас
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <ul class="nav responsive-tab nav-material nav-material-white">
                        <li>
                            <a class="nav-link active" href="{{route('absliders.index')}}"><i class="icon icon-list"></i>Все</a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{route('absliders.create')}}"><i
                                        class="icon icon-plus-circle"></i> Добавить слайдер</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        @include('backend.message.msg')
        <div class="container-fluid animatedParent animateOnce my-3">
            <div class="animated fadeInUpShort">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card no-b shadow">
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    @if($absliders)
                                    <table class="table table-hover ">
                                        <tbody>
                                        @foreach($absliders as $abslider)
                                        <tr class="no-b">
                                            <td class="w-10">
                                                <img src="{{$abslider->getImage()}}" alt="">
                                            </td>
                                            <td>
                                                <h6>{{$abslider->title}}</h6>
                                                <small class="text-muted">{!! str_limit($abslider->content, 50) !!}</small>
                                            </td>
                                            <td>
                                                <span><i class="icon icon-timer"></i> {{$abslider->created_at}}</span>
                                            </td>
                                            <td>
                                                <a href="{{route('absliders.edit', $abslider->id)}}" class="btn-fab btn-fab-sm btn-primary shadow text-white"><i class="icon-pencil"></i></a>
                                                {{Form::open(['route'=>['absliders.destroy', $abslider->id], 'method'=>'delete'])}}
                                                <button type="submit" class="btn btn-danger btn-xs"><i class="icon-remove"></i></button>
                                                {{Form::close()}}
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection