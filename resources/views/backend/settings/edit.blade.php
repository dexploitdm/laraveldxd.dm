@extends('backend.layout')

@section('content')
    <div class="page has-sidebar-left">
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-package"></i>
                            Настройки
                        </h4>
                    </div>
                </div>
                @include('backend.message.errors')
                <div class="row">
                    <ul class="nav responsive-tab nav-material nav-material-white">
                        <li>
                            <a class="nav-link active" href="#"><i
                                        class="icon icon-plus-circle"></i>Редактирование настроек</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        {{Form::open([
           'route'	=>	['settings.update', $setting->id],
           'files'	=>	true,
           'method'	=>	'put'
       ])}}
        <div class="container-fluid animatedParent animateOnce my-3">
            <div class="animated fadeInUpShort">
                <form id="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-md-8 ">
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Название</label>
                                    <input type="text" class="form-control" id="validationCustom01" name="title"
                                           placeholder="Название" value="{{$setting->title}}">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Адрес</label>
                                    <input type="text" class="form-control" id="validationCustom01" name="address"
                                           placeholder="Ссылка проекта" value="{{$setting->address}}">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Телефон</label>
                                    <input type="text" class="form-control" id="validationCustom01" name="phone"
                                           placeholder="Введите номер" value="{{$setting->phone}}">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Время работы</label>
                                    <input type="text" class="form-control" id="validationCustom01" name="time_work"
                                           placeholder="Время работы" value="{{$setting->time_work}}">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="">
                                        <input type="text" class="form-control" id="validationCustom01" name="facebook"
                                               placeholder="Ссылка facebook" value="{{$setting->facebook}}">
                                    </div>
                                    <div class="">
                                        <input type="text" class="form-control" id="validationCustom01" name="vk"
                                               placeholder="Ссылка vk" value="{{$setting->vk}}">
                                    </div>
                                    <div class="">
                                        <input type="text" class="form-control" id="validationCustom01" name="twitter"
                                               placeholder="Ссылка twitter" value="{{$setting->twitter}}">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="">
                                        <input type="text" class="form-control" id="validationCustom01" name="google"
                                               placeholder="Ссылка google" value="{{$setting->google}}">
                                    </div>
                                    <div class="">
                                        <input type="text" class="form-control" id="validationCustom01" name="youtube"
                                               placeholder="Ссылка youtube" value="{{$setting->youtube}}">
                                    </div>
                                    <div class="">
                                        <input type="text" class="form-control" id="validationCustom01" name="github"
                                               placeholder="Ссылка github" value="{{$setting->github}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card mt-4">
                                <h6 class="card-header white">Опубликовать</h6>
                                <div class="card-footer bg-transparent">
                                    <button class="btn btn-primary" type="submit">Сохранить</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-4 imagesworksup">
                                <div class="imagedrozone">
                                    <label>Логотип</label>
                                    <input type="file" id="imageInput" name="logo">
                                    <div class="customdropzone">
                                        <div class="layoutdrop">
                                            <div class="customdropzone_title"><p>Dropzone</p></div>
                                            <div id="preview">
                                                @if($setting->getLogo())
                                                    <img src="{{$setting->getLogo()}}">
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-danger btn-sm" id="removePreview">Удалить</button>
                                </div>
                            </div>
                            <div class="col-md-2 imagesworksup">
                                <div class="input-file-row-1">
                                    <div class="upload-file-container">
                                        <img id="image" src="{{$setting->getBanner()}}" alt="" />
                                        <div class="upload-file-container-text">
                                            <span>Баннер в сайдбаре</span>
                                            <input type="file" name="banner" class="photo" id="imgInput" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Название баннера</label>
                                    <input type="text" class="form-control" id="validationCustom01" name="banner_title"
                                           placeholder="Название" value="{{$setting->banner_title}}">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Ссылка баннера</label>
                                    <input type="text" class="form-control" id="validationCustom01" name="banner_link"
                                           placeholder="Ссылка" value="{{$setting->banner_link}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="productDetails">Текст на баннере</label>
                                <textarea name="banner_text" id="editor1">{{$setting->banner_text}}</textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        {{Form::close()}}
    </div>
    <script>
        var editor = CKEDITOR.replace( 'editor1',{
            filebrowserBrowseUrl : '/elfinder/ckeditor'
        } );
    </script>
@endsection