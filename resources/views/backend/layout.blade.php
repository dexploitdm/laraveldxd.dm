<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{asset('backassets/assets/img/basic/favicon.ico')}}" type="image/x-icon">
    <title>Admin | {{config('app.name')}}</title>
    <!-- CSS -->
    <link rel="stylesheet" href="{{asset('backassets/assets/css/app.css')}}">
    <link rel="stylesheet" href="{{asset('backassets/assets/css/style.css')}}">
    <script src="{{ asset('backassets/ckeditordxd/ckeditor.js') }}"
            type="text/javascript" charset="utf-8" ></script>
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>

    <style>
        .loader {
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            background-color: #F5F8FA;
            z-index: 9998;
            text-align: center;
        }

        .plane-container {
            position: absolute;
            top: 50%;
            left: 50%;
        }
    </style>
</head>
<body class="light">

<div id="app">
    @extends('backend.navigate')
    <!-- Вверхняя часть -->
    <div class="has-sidebar-left">
        <div class="pos-f-t">
            <div class="collapse" id="navbarToggleExternalContent">
                <div class="bg-dark pt-2 pb-2 pl-4 pr-2">
                    <div class="search-bar">
                        <input class="transparent s-24 text-white b-0 font-weight-lighter w-128 height-50" type="text"
                               placeholder="start typing...">
                    </div>
                    <a href="#" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-expanded="false"
                       aria-label="Toggle navigation" class="paper-nav-toggle paper-nav-white active "><i></i></a>
                </div>
            </div>
        </div>
        <div class="sticky">
            <div class="navbar navbar-expand navbar-dark d-flex justify-content-between bd-navbar blue accent-3">
                <div class="relative">
                    <a href="#" data-toggle="offcanvas" class="paper-nav-toggle pp-nav-toggle">
                        <i></i>
                    </a>
                </div>
                <!--Top Menu Start -->
                <div class="navbar-custom-menu p-t-10">
                    <ul class="nav navbar-nav">
                        <!-- Messages-->
                        <li class="dropdown custom-dropdown messages-menu">
                            <a href="#" class="nav-link" data-toggle="dropdown">
                                <i class="icon-message "></i>
                                <span class="badge badge-success badge-mini rounded-circle">4</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu pl-2 pr-2">
                                        <!-- start message -->
                                        <li>
                                            <a href="#">
                                                <div class="avatar float-left">
                                                    <img src="{{asset('backassets/assets/img/dummy/u4.png')}}" alt="">
                                                    <span class="avatar-badge busy"></span>
                                                </div>
                                                <h4>
                                                    Support Team
                                                    <small><i class="icon icon-clock-o"></i> 5 mins</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                        <!-- end message -->
                                        <!-- start message -->
                                        <li>
                                            <a href="#">
                                                <div class="avatar float-left">
                                                    <img src="{{asset('backassets/assets/img/dummy/u1.png')}}" alt="">
                                                    <span class="avatar-badge online"></span>
                                                </div>
                                                <h4>
                                                    Support Team
                                                    <small><i class="icon icon-clock-o"></i> 5 mins</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                        <!-- end message -->
                                        <!-- start message -->
                                        <li>
                                            <a href="#">
                                                <div class="avatar float-left">
                                                    <img src="{{asset('backassets/assets/img/dummy/u2.png')}}" alt="">
                                                    <span class="avatar-badge idle"></span>
                                                </div>
                                                <h4>
                                                    Support Team
                                                    <small><i class="icon icon-clock-o"></i> 5 mins</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                        <!-- end message -->
                                        <!-- start message -->
                                        <li>
                                            <a href="#">
                                                <div class="avatar float-left">
                                                    <img src="{{asset('backassets/assets/img/dummy/u3.png')}}" alt="">
                                                    <span class="avatar-badge busy"></span>
                                                </div>
                                                <h4>
                                                    Support Team
                                                    <small><i class="icon icon-clock-o"></i> 5 mins</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                        <!-- end message -->
                                    </ul>
                                </li>
                                <li class="footer s-12 p-2 text-center"><a href="#">See All Messages</a></li>
                            </ul>
                        </li>
                        <!-- Notifications -->
                        <li class="dropdown custom-dropdown notifications-menu">
                            <a href="#" class=" nav-link" data-toggle="dropdown" aria-expanded="false">
                                <i class="icon-notifications "></i>
                                <span class="badge badge-danger badge-mini rounded-circle">4</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have 10 notifications</li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        <li>
                                            <a href="#">
                                                <i class="icon icon-data_usage text-success"></i> 5 new members joined today
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon icon-data_usage text-danger"></i> 5 new members joined today
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon icon-data_usage text-yellow"></i> 5 new members joined today
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="footer p-2 text-center"><a href="#">View all</a></li>
                            </ul>
                        </li>
                        <!-- Right Sidebar Toggle Button -->
                        <li>
                            <a class="nav-link ml-2" data-toggle="control-sidebar">
                                <i class="icon-tasks "></i>
                            </a>
                        </li>
                        <!-- User Account-->
                        <li class="dropdown custom-dropdown user user-menu">
                            <a href="#" class="nav-link" data-toggle="dropdown">
                                <img src="{{asset('backassets/assets/img/dummy/u8.png')}}" class="user-image" alt="User Image">
                                <i class="icon-more_vert "></i>
                            </a>
                            <div class="dropdown-menu p-4">
                                <div class="row box justify-content-between my-4">
                                    <div class="col">
                                        <a href="#">
                                            <i class="icon-apps purple lighten-2 avatar  r-5"></i>
                                            <div class="pt-1">Статьи</div>
                                        </a>
                                    </div>
                                    <div class="col"><a href="#">
                                            <i class="icon-beach_access pink lighten-1 avatar  r-5"></i>
                                            <div class="pt-1">Профиль</div>
                                        </a></div>
                                    <div class="col">
                                        <a href="#">
                                            <i class="icon-star light-green lighten-1 avatar  r-5"></i>
                                            <div class="pt-1">Теги</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Вверхняя часть END -->
    <!-- Контент -->
    @yield('content')
    <!-- Add the sidebar's background. This div must be placed
             immediately after the control sidebar -->
    <div class="control-sidebar-bg shadow white fixed"></div>
</div>
<!--/#app -->
<script src="{{asset('backassets/assets/js/app.js')}}"></script>
<script src="{{asset('backassets/assets/js/backends.js')}}"></script>
</body>
</html>