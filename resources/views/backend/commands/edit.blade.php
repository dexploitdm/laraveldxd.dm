@extends('backend.layout')

@section('content')
    <div class="page has-sidebar-left">
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-package"></i>
                            Команда
                        </h4>
                    </div>
                </div>
                @include('backend.message.errors')
                <div class="row">
                    <ul class="nav responsive-tab nav-material nav-material-white">
                        <li>
                            <a class="nav-link" href="{{route('commands.index')}}"><i class="icon icon-list"></i>Вся команда</a>
                        </li>
                        <li>
                            <a class="nav-link active" href="#"><i
                                        class="icon icon-plus-circle"></i>Редактирование сотрудника</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        {{Form::open([
           'route'	=>	['commands.update', $command->id],
           'files'	=>	true,
           'method'	=>	'put'
       ])}}
        <div class="container-fluid animatedParent animateOnce my-3">
            <div class="animated fadeInUpShort">
                <form id="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-md-8 ">
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Имя и фамилия</label>
                                    <input type="text" class="form-control" id="validationCustom01" name="title"
                                           placeholder="Имя" value="{{$command->title}}">
                                    <div class="btn_layout">
                                        <div class="">
                                            <input type="text" class="form-control" id="validationCustom01" name="facebook"
                                                   placeholder="Ссылка facebook" value="{{$command->facebook}}">
                                        </div>
                                        <div class="">
                                            <input type="text" class="form-control" id="validationCustom01" name="vk"
                                                   placeholder="Ссылка vk" value="{{$command->vk}}">
                                        </div>
                                        <div class="">
                                            <input type="text" class="form-control" id="validationCustom01" name="twitter"
                                                   placeholder="Ссылка twitter" value="{{$command->twitter}}">
                                        </div>
                                        <div class="">
                                            <input type="text" class="form-control" id="validationCustom01" name="google"
                                                   placeholder="Ссылка google" value="{{$command->google}}">
                                        </div>
                                        <div class="">
                                            <input type="text" class="form-control" id="validationCustom01" name="youtube"
                                                   placeholder="Ссылка youtube" value="{{$command->youtube}}">
                                        </div>
                                        <div class="">
                                            <input type="text" class="form-control" id="validationCustom01" name="github"
                                                   placeholder="Ссылка github" value="{{$command->github}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="imagedrozone sliderimage">
                                        <label>Изображение</label>
                                        <input type="file" id="imageInput" name="image">
                                        <div class="customdropzone">
                                            <div class="layoutdrop">
                                                <div class="customdropzone_title"><p>Dropzone</p></div>
                                                <div id="preview">
                                                    @if($command->getimage())
                                                        <img src="{{$command->getimage()}}">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-danger btn-sm" id="removePreview">Удалить</button>
                                    </div>
                                    <label for="validationCustom01">Должность</label>
                                    <input type="text" class="form-control" id="validationCustom01" name="work"
                                           placeholder="Должнось сотрудника" value="{{$command->work}}"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card mt-4">
                                <h6 class="card-header white">Опубликовать</h6>
                                <div class="card-footer bg-transparent">
                                    <button class="btn btn-primary" type="submit">Сохранить</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        {{Form::close()}}
    </div>
    <script>
        var editor = CKEDITOR.replace( 'editor1',{
            filebrowserBrowseUrl : '/elfinder/ckeditor'
        } );
    </script>
@endsection