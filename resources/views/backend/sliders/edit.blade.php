@extends('backend.layout')

@section('content')
    <div class="page has-sidebar-left">
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-package"></i>
                            Слайдер
                        </h4>
                    </div>
                </div>
                @include('backend.message.errors')
                <div class="row">
                    <ul class="nav responsive-tab nav-material nav-material-white">
                        <li>
                            <a class="nav-link" href="{{route('sliders.index')}}"><i class="icon icon-list"></i>Все слайдеры</a>
                        </li>
                        <li>
                            <a class="nav-link active" href="#"><i
                                        class="icon icon-plus-circle"></i>Редактирование слайдера</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        {{Form::open([
           'route'	=>	['sliders.update', $slider->id],
           'files'	=>	true,
           'method'	=>	'put'
       ])}}
        <div class="container-fluid animatedParent animateOnce my-3">
            <div class="animated fadeInUpShort">
                <form id="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-md-8 ">
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Название</label>
                                    <input type="text" class="form-control" id="validationCustom01" name="title"
                                           placeholder="Надпись на слайдере" value="{{$slider->title}}">
                                    <div class="backgroun">
                                        <label for="validationCustom01">Фон:</label>
                                        <div class="color-picker input-group colorpicker-element focused">
                                            <input type="text" value="{{$slider->background}}" name="background" class="form-control">
                                            <span class="input-group-append">
                                                <span class="input-group-text add-on white">
                                                    <i class="circle" style="background-color: rgb(39, 125, 133);"></i>
                                              </span>
                                            </span>
                                        </div>
                                        <label for="validationCustom01">Цвет заголовка:</label>
                                        <div class="color-picker input-group colorpicker-element focused">
                                            <input type="text" value="{{$slider->color_title}}" name="color_title" class="form-control">
                                            <span class="input-group-append">
                                                <span class="input-group-text add-on white">
                                                    <i class="circle" style="background-color: rgb(39, 125, 133);"></i>
                                              </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="imagedrozone sliderimage">
                                        <label>Изображение</label>
                                        <input type="file" id="imageInput" name="image">
                                        <div class="customdropzone">
                                            <div class="layoutdrop">
                                                <div class="customdropzone_title"><p>Dropzone</p></div>
                                                <div id="preview">
                                                    @if($slider->getimage())
                                                        <img src="{{$slider->getimage()}}">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-danger btn-sm" id="removePreview">Удалить</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card mt-4">
                                <h6 class="card-header white">Опубликовать</h6>
                                <div class="card-footer bg-transparent">
                                    <button class="btn btn-primary" type="submit">Сохранить</button>
                                </div>
                            </div>
                            <div class="btn_layout">
                                <div class="">
                                    <input type="text" class="form-control" id="validationCustom01" name="btn"
                                           placeholder="Название кнопки" value="{{$slider->btn}}">
                                </div>
                                <div class="">
                                    <input type="text" class="form-control" id="validationCustom01" name="btn_links"
                                           placeholder="Ссылка кнопки" value="{{$slider->btn_links}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="productDetails">Контент</label>
                                <textarea name="content" id="editor1">{{$slider->content}}</textarea>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
        {{Form::close()}}
    </div>
    <script>
        var editor = CKEDITOR.replace( 'editor1',{
            filebrowserBrowseUrl : '/elfinder/ckeditor'
        } );
    </script>
@endsection