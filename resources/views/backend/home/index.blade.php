@extends('backend.layout')

@section('content')
    <div class="page has-sidebar-left height-full">
        <header class="blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-box"></i>
                            Панель
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <ul class="nav responsive-tab nav-material nav-material-white" id="v-pills-tab">
                        <li>
                            <a class="nav-link active" id="v-pills-1-tab" data-toggle="pill" href="#v-pills-1">
                                <i class="icon icon-home2"></i>Today</a>
                        </li>
                    </ul>
                    <a class="btn-fab fab-right btn-primary" data-toggle="control-sidebar">
                        <i class="icon icon-menu"></i>
                    </a>
                </div>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce">
            <div class="tab-content pb-3" id="v-pills-tabContent">
                <!--Today Tab Start-->
                <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
                    <div class="row my-3">
                        <div class="col-md-3">
                            <div class="counter-box white r-5 p-3">
                                <div class="p-4">
                                    <div class="float-right">
                                        <span class="icon icon-note-list text-light-blue s-48"></span>
                                    </div>
                                    <div class="counter-title">Web Projects</div>
                                    <h5 class="sc-counter mt-3">1228</h5>
                                </div>
                                <div class="progress progress-xs r-0">
                                    <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25"
                                         aria-valuemin="0" aria-valuemax="128"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="counter-box white r-5 p-3">
                                <div class="p-4">
                                    <div class="float-right">
                                        <span class="icon icon-mail-envelope-open s-48"></span>
                                    </div>
                                    <div class="counter-title ">Premium Themes</div>
                                    <h5 class="sc-counter mt-3">1228</h5>
                                </div>
                                <div class="progress progress-xs r-0">
                                    <div class="progress-bar" role="progressbar" style="width: 50%;" aria-valuenow="25"
                                         aria-valuemin="0" aria-valuemax="128"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="counter-box white r-5 p-3">
                                <div class="p-4">
                                    <div class="float-right">
                                        <span class="icon icon-stop-watch3 s-48"></span>
                                    </div>
                                    <div class="counter-title">Support Requests</div>
                                    <h5 class="sc-counter mt-3">1228</h5>
                                </div>
                                <div class="progress progress-xs r-0">
                                    <div class="progress-bar" role="progressbar" style="width: 75%;" aria-valuenow="25"
                                         aria-valuemin="0" aria-valuemax="128"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="counter-box white r-5 p-3">
                                <div class="p-4">
                                    <div class="float-right">
                                        <span class="icon icon-inbox-document-text s-48"></span>
                                    </div>
                                    <div class="counter-title">Support Requests</div>
                                    <h5 class="sc-counter mt-3">550</h5>
                                </div>
                                <div class="progress progress-xs r-0">
                                    <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25"
                                         aria-valuemin="0" aria-valuemax="128"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row row-eq-height">
                        <!-- Product Widget Start -->
                        <div class="col-md-6">
                            <div class="card no-b r-5 my-3">
                                <div class="card-body">
                                    <h5 class="card-title">Новые подписчики <span class="badge badge-success r-3">30+</span>
                                    </h5>
                                    <p>There are 30 new followers</p>
                                    <div class="avatar-group">
                                        <figure class="avatar">
                                            <img src="assets/img/dummy/u4.png" alt=""></figure>
                                        <figure class="avatar">
                                            <span class="avatar-letter avatar-letter-l circle"></span>
                                        </figure>
                                        <figure class="avatar">
                                            <img src="assets/img/dummy/u5.png" alt=""></figure>
                                        <figure class="avatar">
                                            <img src="assets/img/dummy/u6.png" alt=""></figure>
                                        <figure class="avatar">
                                            <img src="assets/img/dummy/u7.png" alt="">
                                        </figure>
                                        <figure class="avatar">
                                            <span class="avatar-letter avatar-letter-a circle"></span>
                                        </figure>
                                        <figure class="avatar">
                                            <span class="avatar-letter avatar-letter-b circle"></span>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                            <div class="card no-b r-5 my-3">
                                <div class="bg-primary text-white lighten-2 r-5">
                                    <div class="pt-5 pb-0 pl-4 pr-4">
                                        <div class="lightSlider masonry-container" data-item="1" data-item-md="1"
                                             data-item-sm="1" data-auto="true" data-pause="6000" data-pager="false" data-controls="false" data-loop="true">
                                            <div>
                                                <h5 class="font-weight-normal s-14">Followers Increased</h5>
                                                <div class="my-5">
                                                  <span>
                                               Today 30%</span>
                                                    <div class="progress" style="height: 3px;">
                                                        <div class="progress-bar bg-success" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="my-5">
                                                  <span>
                                               Yesterday 10%</span>
                                                    <div class="progress" style="height: 3px;">
                                                        <div class="progress-bar bg-success" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <canvas width="378" height="140" data-chart="spark" data-chart-type="bar"
                                                        data-dataset="[[28,530,200,430,28,530,200,430,28,530,200,430,28,530,200,430,28,530,200,430]]"
                                                        data-labels="['a','b','c','d','a','b','c','d','a','b','c','d','a','b','c','d','a','b','c','d']"
                                                        data-dataset-options="[
                                                        { borderColor:  'rgba(54, 162, 235, 1)', backgroundColor: 'rgba(54, 162, 235,1)'},
                                                        ]">
                                                </canvas>
                                            </div>
                                            <div>
                                                <h5 class="font-weight-normal s-14">Followers Increased</h5>
                                                <div class="my-5">
                                                  <span>
                                               Today 30%</span>
                                                    <div class="progress" style="height: 3px;">
                                                        <div class="progress-bar bg-success" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="my-5">
                                                  <span>
                                               Yesterday 10%</span>
                                                    <div class="progress" style="height: 3px;">
                                                        <div class="progress-bar bg-success" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <canvas width="378" height="140" data-chart="spark" data-chart-type="line"
                                                        data-dataset="[[28,530,200,430,28,530,200,430,28,530,200,430,28,530,200,430,28,530,200,430]]"
                                                        data-labels="['a','b','c','d','a','b','c','d','a','b','c','d','a','b','c','d','a','b','c','d']"
                                                        data-dataset-options="[
                                                        { borderColor:  'rgba(54, 162, 235, 1)', backgroundColor: 'rgba(54, 162, 235,1)'},
                                                        ]">
                                                </canvas>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card my-3 no-b">
                                <div class="card-header white b-0 p-3">
                                    <div class="card-handle">
                                        <a data-toggle="collapse" href="#salesCard" aria-expanded="false"
                                           aria-controls="salesCard">
                                            <i class="icon-menu"></i>
                                        </a>
                                    </div>
                                    <h4 class="card-title">Daily Sale Report</h4>
                                    <small class="card-subtitle mb-2 text-muted">Items purchase by users.</small>
                                </div>
                                <div class="collapse show" id="salesCard">
                                    <div class="card-body p-0">
                                        <div class="table-responsive">
                                            <table class="table table-hover earning-box">
                                                <thead class="bg-light">
                                                <tr>
                                                    <th colspan="2">Client Name</th>
                                                    <th>Item Purchased</th>
                                                    <th>Price</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td class="w-10">
                                                        <a href="panel-page-profile.html" class="avatar avatar-lg">
                                                            <img src="assets/img/dummy/u6.png" alt="">
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <h6>Sara Kamzoon</h6>
                                                        <small class="text-muted">Marketing Manager</small>
                                                    </td>
                                                    <td>25</td>
                                                    <td>$250</td>
                                                </tr>
                                                <tr>
                                                    <td class="w-10">
                                                        <a href="panel-page-profile.html" class="avatar avatar-lg">
                                                            <img src="assets/img/dummy/u7.png" alt="">
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <h6>Sara Kamzoon</h6>
                                                        <small class="text-muted">Marketing Manager</small>
                                                    </td>
                                                    <td>25</td>
                                                    <td>$250</td>
                                                </tr>
                                                <tr>
                                                    <td class="w-10">
                                                        <a href="panel-page-profile.html" class="avatar avatar-lg">
                                                            <img src="assets/img/dummy/u9.png" alt="">
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <h6>Sara Kamzoon</h6>
                                                        <small class="text-muted">Marketing Manager</small>
                                                    </td>
                                                    <td>25</td>
                                                    <td>$250</td>
                                                </tr>
                                                <tr>
                                                    <td class="w-10">
                                                        <a href="panel-page-profile.html" class="avatar avatar-lg">
                                                            <img src="assets/img/dummy/u11.png" alt="">
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <h6>Sara Kamzoon</h6>
                                                        <small class="text-muted">Marketing Manager</small>
                                                    </td>
                                                    <td>25</td>
                                                    <td>$250</td>
                                                </tr>
                                                <tr>
                                                    <td class="w-10">
                                                        <a href="panel-page-profile.html" class="avatar avatar-lg">
                                                            <img src="assets/img/dummy/u12.png" alt="">
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <h6>Sara Kamzoon</h6>
                                                        <small class="text-muted">Marketing Manager</small>
                                                    </td>
                                                    <td>25</td>
                                                    <td>$250</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Today Tab End-->
            </div>
        </div>
    </div>
@endsection