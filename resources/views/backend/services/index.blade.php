@extends('backend.layout')

@section('content')
    <div class="page has-sidebar-left">
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-package"></i>
                            Услуги
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <ul class="nav responsive-tab nav-material nav-material-white">
                        <li>
                            <a class="nav-link active" href="{{route('services.index')}}"><i class="icon icon-list"></i>Все</a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{route('services.create')}}"><i
                                        class="icon icon-plus-circle"></i> Добавить услугу</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        @include('backend.message.msg')
        <div class="container-fluid animatedParent animateOnce my-3">
            <div class="animated fadeInUpShort">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card no-b shadow">
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    @if($services)
                                    <table class="table table-hover ">
                                        <tbody>
                                        @foreach($services as $service)
                                        <tr class="no-b">
                                            <td class="w-10">
                                                <img src="{{$service->getImage()}}" alt="">
                                            </td>
                                            <td>
                                                <h6>{{$service->title}}</h6>
                                                <small class="text-muted"></small>
                                            </td>
                                            <td>
                                                <label style="display: inline-block;
                                                    vertical-align: 14px;
                                                    padding-right: 15px;">Фон</label>
                                                <div style="    height: 37px;
                                                        width: 37px;
                                                        border: 1px dashed #404040;
                                                        display: inline-block;
                                                        background-color: {{$service->background}};">
                                                </div>
                                            </td>
                                            <td>
                                                <span><i class="icon icon-timer"></i> {{$service->created_at}}</span>
                                            </td>
                                            <td>
                                                <a href="{{route('services.edit', $service->id)}}" class="btn-fab btn-fab-sm btn-primary shadow text-white"><i class="icon-pencil"></i></a>
                                                {{Form::open(['route'=>['services.destroy', $service->id], 'method'=>'delete'])}}
                                                <button type="submit" class="btn btn-danger btn-xs"><i class="icon-remove"></i></button>
                                                {{Form::close()}}
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection