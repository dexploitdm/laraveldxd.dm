@extends('backend.layout')

@section('content')
    <div class="page has-sidebar-left">
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-package"></i>
                            Пользователи
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <ul class="nav responsive-tab nav-material nav-material-white">
                        <li>
                            <a class="nav-link active" href="{{route('users.index')}}"><i class="icon icon-list"></i>Все</a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{route('users.create')}}"><i
                                        class="icon icon-plus-circle"></i> Добавить пользователя</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        @include('backend.message.msg')
        <div class="container-fluid animatedParent animateOnce my-3">
            <div class="animated fadeInUpShort">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card no-b shadow">
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    @if($users)
                                    <table class="table table-hover ">
                                        <tbody>
                                        @foreach($users as $user)
                                        <tr class="no-b">
                                            <td class="w-10">
                                                <img src="{{$user->getImage()}}" alt="">
                                            </td>
                                            <td>
                                                <h6>{{$user->name}}</h6>
                                                <small class="text-muted">@if($user->status == 1)Админ @else Пользователь @endif</small>
                                            </td>
                                            @if($user->status == 1)
                                                <td><span class="badge badge-danger">Забанен</span></td>
                                            @else
                                                <td><span class="badge badge-success">Свободный</span></td>
                                            @endif
                                            @if($user->is_admin == 1)
                                                <td>
                                                    <span class="badge badge-dark r-20">Админ</span></td>
                                            @else
                                                <td><span class="badge badge-light r-20">Простой пользователь</span></td>
                                            @endif
                                            <td>
                                                <span><i class="icon icon-timer"></i>{{$user->created_at}}</span>
                                            </td>
                                            <td>
                                                <a href="{{route('users.edit', $user->id)}}" class="btn-fab btn-fab-sm btn-primary shadow text-white"><i class="icon-pencil"></i></a>
                                                {{Form::open(['route'=>['users.destroy', $user->id], 'method'=>'delete'])}}
                                                <button type="submit" class="btn btn-danger btn-xs"><i class="icon-remove"></i></button>
                                                {{Form::close()}}
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <nav class="pt-3" aria-label="Page navigation">
                    <ul class="pagination">

                    </ul>
                </nav>
            </div>
        </div>
    </div>
@endsection