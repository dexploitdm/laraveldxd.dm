@extends('backend.layout')

@section('content')
    <div class="page has-sidebar-left">
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-package"></i>
                            Пользователи
                        </h4>
                    </div>
                </div>
                @include('backend.message.errors')
                <div class="row">
                    <ul class="nav responsive-tab nav-material nav-material-white">
                        <li>
                            <a class="nav-link" href="{{route('users.index')}}"><i class="icon icon-list"></i>Все пользователи</a>
                        </li>
                        <li>
                            <a class="nav-link active" href="#"><i
                                        class="icon icon-plus-circle"></i>Добавление пользователя</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        {{Form::open([
        'route'=>'users.store','files'=>true])}}
        <div class="container-fluid animatedParent animateOnce my-3">
            <div class="animated fadeInUpShort">
                <form id="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-md-8 ">
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Имя</label>
                                    <input type="text" class="form-control" id="validationCustom01" name="name"
                                           placeholder="Название статьи" value="{{old('name')}}">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Email</label>
                                    <input type="text" class="form-control" id="validationCustom01" name="email"
                                           placeholder="Email" value="{{old('email')}}">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Пароль</label>
                                    <input type="text" class="form-control" id="validationCustom01" name="password"
                                           placeholder="Пароль" value="{{old('password')}}">
                                </div>
                                <div class="col-md-3 mb-3 checkadmin">
                                    <label>Админ</label>
                                    <div class="material-switch float-right">
                                        <input id="someSwitchOptionDefault" name="is_admin" type="checkbox" value="0">
                                        <label for="someSwitchOptionDefault" class="bg-secondary"></label>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3 checkban">
                                    <label>Заблокировать</label>
                                    <div class="material-switch float-right">
                                        <input id="someSwitchOptionSuccess" name="status" type="checkbox" value="0">
                                        <label for="someSwitchOptionSuccess" class="bg-success"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                            </div>
                    </div>
                        <div class="col-md-3">
                            <div class="card mt-4">
                                <h6 class="card-header white">Опубликовать</h6>
                                <div class="card-footer bg-transparent">
                                    <button class="btn btn-primary" type="submit">Сохранить</button>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="imagedrozone">
                                    <label>Аватар</label>
                                    <input type="file" id="imageInput" name="avatar">
                                    <div class="customdropzone">
                                        <div class="layoutdrop">
                                            <div class="customdropzone_title"><p>Dropzone</p></div>
                                            <div id="preview"></div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-danger btn-sm" id="removePreview">Удалить</button>
                                </div>
                            </div>
                        </div>
                </form>
            </div>
        </div>
        {{Form::close()}}
    </div>
@endsection