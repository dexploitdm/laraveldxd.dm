@extends('backend.layout')

@section('content')
    <div class="page has-sidebar-left">
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-package"></i>
                            Проекты
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <ul class="nav responsive-tab nav-material nav-material-white">
                        <li>
                            <a class="nav-link active" href="{{route('works_up.index')}}"><i class="icon icon-list"></i>Все</a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{route('works_up.create')}}"><i
                                        class="icon icon-plus-circle"></i> Добавить проект</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        @include('backend.message.msg')
        <div class="container-fluid animatedParent animateOnce my-3">
            <div class="animated fadeInUpShort">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card no-b shadow">
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    @if($works)
                                    <table class="table table-hover ">
                                        <tbody>
                                        @foreach($works as $work)
                                        <tr class="no-b">
                                            <td class="w-10">
                                                <img src="{{$work->getImage()}}" alt="">
                                            </td>
                                            <td class="w-10">
                                                <img src="{{$work->getImageTwo()}}" alt="">
                                            </td>
                                            <td class="w-10">
                                                <img src="{{$work->getImageThree()}}" alt="">
                                            </td>
                                            <td class="w-10">
                                                <img src="{{$work->getImageBlocks()}}" alt="">
                                            </td>
                                            <td>
                                                <h6>{{$work->title}}</h6>
                                                <small class="text-muted"></small>
                                            </td>
                                            @if($work->is_slider == 1)
                                            <td><span class="badge badge-success">Вид слайда</span></td>
                                            @else
                                                <td><span class="badge badge-danger">Обычный вид</span></td>
                                            @endif
                                            <td>
                                                <span><i class="icon icon-timer"></i> {{$work->created_at}}</span>
                                            </td>
                                            <td>
                                                <a href="{{route('works_up.edit', $work->id)}}" class="btn-fab btn-fab-sm btn-primary shadow text-white"><i class="icon-pencil"></i></a>
                                                {{Form::open(['route'=>['works_up.destroy', $work->id], 'method'=>'delete'])}}
                                                <button type="submit" class="btn btn-danger btn-xs"><i class="icon-remove"></i></button>
                                                {{Form::close()}}
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <nav class="pt-3" aria-label="Page navigation">
                    {{$works->links()}}
                </nav>
            </div>
        </div>
    </div>
@endsection