@extends('backend.layout')

@section('content')
    <div class="page has-sidebar-left">
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-package"></i>
                            Проекты
                        </h4>
                    </div>
                </div>
                @include('backend.message.errors')
                <div class="row">
                    <ul class="nav responsive-tab nav-material nav-material-white">
                        <li>
                            <a class="nav-link" href="{{route('works_up.index')}}"><i class="icon icon-list"></i>Все проекты</a>
                        </li>
                        <li>
                            <a class="nav-link active" href="#"><i
                                        class="icon icon-plus-circle"></i>Редактирование проекта</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        {{Form::open([
            'route'	=>	['works_up.update', $work->id],
            'files'	=>	true,
            'method'	=>	'put'
	    ])}}
        <div class="container-fluid animatedParent animateOnce my-3">
            <div class="animated fadeInUpShort">
                <form id="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-md-8 ">
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Название проекта</label>
                                    <input type="text" class="form-control" id="validationCustom01" name="title"
                                           placeholder="Название проекта" value="{{$work->title}}">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="">
                                        <div class="card-title">Дата</div>
                                        <div class="input-group focused">
                                            <input type="text" class="date-time-picker form-control" data-options="{&quot;timepicker&quot;:false, &quot;format&quot;:&quot;d/m/y&quot;}"
                                                   name="date" value="{{$work->date}}">
                                            <span class="input-group-append">
                                                <span class="input-group-text add-on white">
                                                    <i class="icon-calendar"></i>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Ссылка на проект</label>
                                    <input type="text" class="form-control" id="validationCustom01" name="links"
                                           placeholder="Ссылка проекта" value="{{$work->links}}">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Ссылка на репозиторий</label>
                                    <input type="text" class="form-control" id="validationCustom01" name="github"
                                           placeholder="Ссылка на репозиторий" value="{{$work->github}}">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Технология</label>
                                    <input type="text" class="form-control" id="validationCustom01" name="technology"
                                           placeholder="Технология" value="{{$work->technology}}">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="category">Категория</label>
                                    {{Form::select('filter_id',
                                        $filters,
                                        $work->getFilterID(),
                                        ['class' => 'custom-select form-control'])}}
                                    <div class="invalid-feedback">
                                        Please provide a valid category.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card mt-4">
                                <h6 class="card-header white">Опубликовать</h6>
                                <div class="card-footer bg-transparent">
                                    <button class="btn btn-primary" type="submit">Сохранить</button>
                                </div>
                            </div>
                            <div class="viewsliders">
                                <label>Отобразить в блочном виде</label>
                                <div class="material-switch float-right">
                                    {{Form::checkbox('is_slider', '0', $work->is_slider, ['id'=>'someSwitchOptionDefault'])}}
                                    <label for="someSwitchOptionDefault" class="bg-secondary"></label>
                                </div>
                            </div>
                            <div class="viewsliders">
                                <label>Отобразить в блок с изображениями</label>
                                <div class="material-switch">
                                    {{Form::checkbox('blocks_image', '0', $work->blocks_image, ['id'=>'sw5'])}}
                                    <label for="sw5" class="bg-default"></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-4 imagesworksup">
                                <div class="imagedrozone">
                                    <label>Основное изображение</label>
                                    <input type="file" id="imageInput" name="image">
                                    <div class="customdropzone">
                                        <div class="layoutdrop">
                                            <div class="customdropzone_title"><p>Dropzone</p></div>
                                            <div id="preview">
                                                @if($work->getimage())
                                                    <img src="{{$work->getimage()}}">
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-danger btn-sm" id="removePreview">Удалить</button>
                                </div>
                            </div>
                            <div class="col-md-2 imagesworksup">
                                <div class="input-file-row-1">
                                    <div class="upload-file-container">
                                        <img id="image" src="{{$work->getImageTwo()}}" alt="" />
                                        <div class="upload-file-container-text">
                                            <span>Изображение 2</span>
                                            <input type="file" name="image_two" class="photo" id="imgInput" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 imagesworksup">
                                <div class="input-file-row-1">
                                    <div class="upload-file-container">
                                        <img id="image3" src="{{$work->getImageThree()}}" alt="" />
                                        <div class="upload-file-container-text">
                                            <span>Изображение 3</span>
                                            <input type="file" name="image_three" class="photo" id="imgInput3" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 imagesworksup">
                                <div class="input-file-row-1">
                                    <div class="upload-file-container">
                                        <img id="imageBlocks" src="{{$work->getImageBlocks()}}" alt="" />
                                        <div class="upload-file-container-text">
                                            <span>Изображение блока</span>
                                            <input type="file" name="image_blocks" class="photo" id="imgInputBlocks" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="productDetails">Контент</label>
                                <textarea name="content" id="editor1">{{$work->content}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="productDetails">Текст блоков</label>
                                <textarea name="text_block" id="editor_blocks">{{$work->text_block}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="productDetails">Keywords</label>
                                <textarea name="keywords" id="editor2">{{$work->keywords}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="productDetails">Мини описание</label>
                                <textarea name="desc" id="editorDesc">{{$work->desc}}</textarea>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
        {{Form::close()}}
    </div>
    <script>
        var editor = CKEDITOR.replace( 'editor1',{
            filebrowserBrowseUrl : '/elfinder/ckeditor'
        } );
        var editor = CKEDITOR.replace( 'editor_blocks',{
            filebrowserBrowseUrl : '/elfinder/ckeditor'
        } );
        var editor = CKEDITOR.replace( 'editor2',{
            filebrowserBrowseUrl : '/elfinder/ckeditor'
        } );
        var editor = CKEDITOR.replace( 'editorDesc',{
            filebrowserBrowseUrl : '/elfinder/ckeditor'
        } );
    </script>
@endsection