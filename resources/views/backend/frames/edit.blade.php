@extends('backend.layout')

@section('content')
    <div class="page has-sidebar-left">
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-package"></i>
                            Frameworks и Cms
                        </h4>
                    </div>
                </div>
                @include('backend.message.errors')
                <div class="row">
                    <ul class="nav responsive-tab nav-material nav-material-white">
                        <li>
                            <a class="nav-link" href="{{route('frames.index')}}"><i class="icon icon-list"></i>Все интсрументы</a>
                        </li>
                        <li>
                            <a class="nav-link active" href="#"><i
                                        class="icon icon-plus-circle"></i>Редактирование инструмента</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        {{Form::open([
           'route'	=>	['frames.update', $frame->id],
           'files'	=>	true,
           'method'	=>	'put'
       ])}}
        <div class="container-fluid animatedParent animateOnce my-3">
            <div class="animated fadeInUpShort">
                <form id="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-md-8 ">
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Название</label>
                                    <input type="text" class="form-control" id="validationCustom01" name="title"
                                           placeholder="Надпись на слайдере" value="{{$frame->title}}">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="imagedrozone sliderimage">
                                        <label>Изображение</label>
                                        <input type="file" id="imageInput" name="image">
                                        <div class="customdropzone">
                                            <div class="layoutdrop">
                                                <div class="customdropzone_title"><p>Dropzone</p></div>
                                                <div id="preview">
                                                    @if($frame->getimage())
                                                        <img src="{{$frame->getimage()}}">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-danger btn-sm" id="removePreview">Удалить</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card mt-4">
                                <h6 class="card-header white">Опубликовать</h6>
                                <div class="card-footer bg-transparent">
                                    <button class="btn btn-primary" type="submit">Сохранить</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        {{Form::close()}}
    </div>
@endsection