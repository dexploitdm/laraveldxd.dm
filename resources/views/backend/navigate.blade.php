<!-- Навигация -->
<aside class="main-sidebar fixed offcanvas shadow">
    <section class="sidebar">
        <div class="w-80px mt-3 mb-3 ml-3">
            <img src="{{asset('backassets/assets/img/basic/logo.png')}}" alt="">
        </div>
        <div class="relative">
            <a data-toggle="collapse" href="#userSettingsCollapse" role="button" aria-expanded="false"
               aria-controls="userSettingsCollapse" class="btn-fab btn-fab-sm fab-right fab-top btn-primary shadow1 ">
                <i class="icon icon-cogs"></i>
            </a>
            <div class="user-panel p-3 light mb-2">
                <div>
                    <div class="float-left image">
                        <img class="user_avatar" src="{{asset('backassets/assets/img/dummy/u2.png')}}" alt="User Image">
                    </div>
                    <div class="float-left info">
                        <h6 class="font-weight-light mt-2 mb-1">Dexploitdm</h6>
                        <a href="#"><i class="icon-circle text-primary blink"></i> Online</a>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="collapse multi-collapse" id="userSettingsCollapse">
                    <div class="list-group mt-3 shadow">
                        @foreach($takeAdminSetting as $admin)
                        <a href="/backend/users/{{$admin->id}}/edit" class="list-group-item list-group-item-action ">
                            <i class="mr-2 icon-umbrella text-blue"></i>Учетные данные
                        </a>
                        @endforeach
                        <a href="#" class="list-group-item list-group-item-action"><i
                                    class="mr-2 icon-cogs text-yellow"></i>Настройки</a>
                        <a href="{{route('home')}}" target="_blank" class="list-group-item list-group-item-action"><i
                              class="mr-2 icon-house text-green"></i>Перейти на сайт</a>
                    </div>
                </div>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="header"><strong>Разделы</strong></li>

            <li class="treeview"><a href="#">
                    <i class="icon icon icon-package blue-text s-18"></i>
                    <span>Статьи</span>
                    <span class="badge r-3 badge-primary pull-right">4</span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('posts.index')}}"><i class="icon icon-circle-o"></i>Все статьи</a>
                    </li>
                    <li><a href="{{route('posts.create')}}"><i class="icon icon-add"></i>Добавить новую </a>
                    </li>
                    <li>
                        <a href="{{route('categories.index')}}">
                            <i class="icon icon-widgets amber-text s-14"></i> <span>Категории</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="treeview"><a href="#">
                    <i class="icon icon icon-package blue-text s-18"></i>
                    <span>Проекты</span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('works_up.index')}}"><i class="icon icon-circle-o"></i>Все проекты</a>
                    </li>
                    <li><a href="{{route('works_up.create')}}"><i class="icon icon-add"></i>Добавить проект</a></li>
                    <li>
                        <a href="{{route('filters.index')}}">
                            <i class="icon icon-widgets amber-text s-14"></i> <span>Фильтры</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="treeview"><a href="#"><i class="icon icon-account_box light-green-text s-18"></i>Пользователи<i
                            class="icon icon-angle-left s-18 pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{route('users.index')}}"><i class="icon icon-circle-o"></i>Все</a>
                    </li>
                    <li><a href="{{route('users.create')}}"><i class="icon icon-add"></i>Добавить</a></li>
                </ul>
            </li>
            <li class="treeview no-b">
                <a href="#">
                    <i class="icon icon-package light-green-text s-18"></i>
                    <span>Коментарии</span>
                    <span class="badge r-3 badge-success pull-right">{{$newCommentsCount}}</span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('comments')}}"><i class="icon icon-circle-o"></i>Все коментарии</a></li>
                </ul>
            </li>
            <li class="header light mt-3"><strong>Компоненты</strong></li>
            <li class="treeview ">
                <a href="#">
                    <i class="icon icon-package text-lime s-18"></i> <span>Теги</span>
                    <i class="icon icon-angle-left s-18 pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('tags.index')}}"><i class="icon icon-circle-o"></i>Все теги</a>
                    </li>
                    <li><a href="{{route('tags.create')}}"><i class="icon icon-add"></i>Добавить</a></li>

                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="icon icon-documents3 text-blue s-18"></i> <span>Страницы</span>
                    <i class="icon icon-angle-left s-18 pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="#"><i class="icon icon-documents3"></i>О нас
                            <i class="icon icon-angle-left s-18 pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="{{route('abouts.index')}}"><i class="icon icon-documents3"></i>Основной
                                </a>
                            </li>
                            <li>
                                <a href="{{route('absliders.index')}}"><i class="icon icon-documents3"></i>Слайдер
                                </a>
                            </li>
                            <li>
                                <a href="{{route('commands.index')}}"><i class="icon icon-documents3"></i>Команда
                                </a>
                            </li>
                            <li>
                                <a href="{{route('frames.index')}}"><i class="icon icon-documents3"></i>Frameworks и cms
                                </a>
                            </li>
                        </ul>

                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="icon icon-goals-1 amber-text s-18"></i> <span>Услуги</span>
                    <i class="icon icon-angle-left s-18 pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{route('services.index')}}">
                            <i class="icon icon-widgets amber-text s-14"></i> <span>Все услуги</span>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="treeview ">
                <a href="{{route('subscribers.index')}}">
                    <i class="icon icon-wpforms light-green-text s-18 "></i> <span>Подписка</span>
                    <span class="badge r-3 badge-success pull-right">{{$subsCount}}</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="icon icon-goals-1 amber-text s-18"></i> <span>Слайдеры</span>
                    <i class="icon icon-angle-left s-18 pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('sliders.index')}}">
                            <i class="icon icon-widgets amber-text s-14"></i> <span>Все слайдеры</span>
                        </a>
                    </li>
                    <li><a href="{{route('sliders.create')}}"><i class="icon icon-add"></i>Добавить</a></li>
                </ul>
            </li>
            <li class="treeview ">
                <a href="{{route('menus.index')}}">
                    <i class="icon icon-wpforms light-green-text s-18 "></i> <span>Меню</span>
                </a>
            </li>
            <li class="treeview ">
                <a href="{{route('settings.index')}}">
                    <i class="icon icon-wpforms light-green-text s-18 "></i> <span>Настройки</span>
                </a>
            </li>
        </ul>
    </section>
</aside>
<!--Навигация End-->