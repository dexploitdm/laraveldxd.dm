@extends('backend.layout')

@section('content')
    <div class="page has-sidebar-left">
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-package"></i>
                            Статьи
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <ul class="nav responsive-tab nav-material nav-material-white">
                        <li>
                            <a class="nav-link active" href="{{route('posts.index')}}"><i class="icon icon-list"></i>Все</a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{route('posts.create')}}"><i
                                        class="icon icon-plus-circle"></i> Добавить статью</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        @include('backend.message.msg')
        <div class="container-fluid animatedParent animateOnce my-3">
            <div class="animated fadeInUpShort">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card no-b shadow">
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    @if($posts)
                                    <table class="table table-hover ">
                                        <tbody>
                                        @foreach($posts as $post)
                                        <tr class="no-b">
                                            <td class="w-10">
                                                <img src="{{$post->getImage()}}" alt="">
                                            </td>
                                            <td>
                                                <h6>{{$post->title}}</h6>
                                                <small class="text-muted">{{$post->category->title}}</small>
                                            </td>
                                            <td>{{$post->getTagsTitles()}}</td>
                                            @if($post->status == 1)
                                            <td><span class="badge badge-success">Опубликовано</span></td>
                                            @else
                                                <td><span class="badge badge-danger">Не опубликовано</span></td>
                                            @endif
                                            @if($post->is_featured == 1)
                                                <td>
                                                    <span class="badge badge-dark r-20">Рекомендовано</span></td>
                                            @else
                                                <td><span class="badge badge-light r-20">Не рекомендовано</span></td>
                                            @endif
                                            <td>
                                                <span><i class="icon icon-timer"></i> {{$post->getDate()}}</span>
                                            </td>
                                            <td>
                                                <a href="{{route('posts.edit', $post->id)}}" class="btn-fab btn-fab-sm btn-primary shadow text-white"><i class="icon-pencil"></i></a>
                                                {{Form::open(['route'=>['posts.destroy', $post->id], 'method'=>'delete'])}}
                                                <button type="submit" class="btn btn-danger btn-xs"><i class="icon-remove"></i></button>
                                                {{Form::close()}}
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <nav class="pt-3" aria-label="Page navigation">
                    {{$posts->links()}}
                </nav>
            </div>
        </div>
    </div>
@endsection