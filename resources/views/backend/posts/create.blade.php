@extends('backend.layout')

@section('content')
    <div class="page has-sidebar-left">
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-package"></i>
                            Статьи
                        </h4>
                    </div>
                </div>
                @include('backend.message.errors')
                <div class="row">
                    <ul class="nav responsive-tab nav-material nav-material-white">
                        <li>
                            <a class="nav-link" href="{{route('posts.index')}}"><i class="icon icon-list"></i>Все статьи</a>
                        </li>
                        <li>
                            <a class="nav-link active" href="#"><i
                                        class="icon icon-plus-circle"></i>Добавление статьи</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        {{Form::open([
        'route'=>'posts.store','files'=>true])}}
        <div class="container-fluid animatedParent animateOnce my-3">
            <div class="animated fadeInUpShort">
                <form id="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-md-8 ">
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Название</label>
                                    <input type="text" class="form-control" id="validationCustom01" name="title"
                                           placeholder="Название статьи" value="{{old('title')}}">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="card-body b-b">
                                        <div class="card-title">Дата</div>
                                        <div class="input-group focused">
                                            <input type="text" class="date-time-picker form-control" data-options="{&quot;timepicker&quot;:false, &quot;format&quot;:&quot;d/m/y&quot;}"
                                                   name="date" value="{{old('date')}}">
                                            <span class="input-group-append">
                                                <span class="input-group-text add-on white">
                                                    <i class="icon-calendar"></i>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="category">Категория</label>
                                    {{Form::select('category_id',
                                       $categories,
                                       null,
                                       ['class' => 'custom-select form-control'])}}
                                    <div class="invalid-feedback">
                                        Please provide a valid category.
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label>Черновик</label>
                                    <div class="material-switch float-right">
                                        <input id="someSwitchOptionDefault" name="status" type="checkbox" value="0">
                                        <label for="someSwitchOptionDefault" class="bg-secondary"></label>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label>Рекомендовать</label>
                                    <div class="material-switch float-right">
                                        <input id="someSwitchOptionSuccess" name="is_featured" type="checkbox" value="0">
                                        <label for="someSwitchOptionSuccess" class="bg-success"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tags">Теги</label><br>
                                {{Form::select('tags[]',
                                $tags,
                                null,
                                ['class' => 'form-control select2', 'multiple'=>'multiple','data-placeholder'=>'Выберите теги'])}}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card mt-4">
                                <h6 class="card-header white">Опубликовать</h6>
                                <div class="card-footer bg-transparent">
                                    <button class="btn btn-primary" type="submit">Сохранить</button>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="imagedrozone">
                                    <label>Изображение</label>
                                    <input type="file" id="imageInput" name="image">
                                    <div class="customdropzone">
                                        <div class="layoutdrop">
                                            <div class="customdropzone_title"><p>Dropzone</p></div>
                                            <div id="preview"></div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-danger btn-sm" id="removePreview">Удалить</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="productDetails">Контент</label>
                                <textarea name="content" id="editor1">{{old('content')}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="productDetails">Keywords</label>
                                <textarea name="keywords" id="editor2">{{old('keywords')}}</textarea>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
        {{Form::close()}}
    </div>
    <script>
        var editor = CKEDITOR.replace( 'editor1',{
            filebrowserBrowseUrl : '/elfinder/ckeditor'
        } );
    </script>
    <script>
        var editor = CKEDITOR.replace( 'editor2',{
            filebrowserBrowseUrl : '/elfinder/ckeditor'
        } );
    </script>
@endsection