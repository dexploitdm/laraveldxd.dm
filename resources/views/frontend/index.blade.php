@extends('frontend.layout')
@section('content')
    @if($sliders)
    <div class="crumina-module crumina-module-slider container-full-width">
        <div class="swiper-container main-slider navigation-center-both-sides" data-effect="fade">
            <div class="swiper-wrapper">
                @foreach($sliders as $slider)
                     <?php $count++; ?>
                    <?php if($count == 1): ?>
                        <div class="swiper-slide bg-1 main-slider-bg-light">
                        <div class="container">
                            <div class="row table-cell">
                                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-sm-offset-0 col-xs-12">
                                    <div class="slider-content align-center">
                                        <h1 class="slider-content-title with-decoration" data-swiper-parallax="-100">
                                            {{$slider->title}}

                                            <svg class="first-decoration utouch-icon utouch-icon-arrow-left"><use xlink:href="#utouch-icon-arrow-left"></use></svg>

                                            <svg class="second-decoration utouch-icon utouch-icon-arrow-left"><use xlink:href="#utouch-icon-arrow-left"></use></svg>

                                        </h1>
                                        <h6 class="slider-content-text" data-swiper-parallax="-200">{!! $slider->content !!}
                                        </h6>
                                        @if($slider->btn)
                                        <div class="main-slider-btn-wrap" data-swiper-parallax="-300">
                                            <a href="{{$slider->btn_links}}" class="btn btn-border btn--with-shadow c-primary">
                                                {{$slider->btn}}
                                            </a>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="slider-thumb" style="text-align: center" data-swiper-parallax="-400" data-swiper-parallax-duration="600">
                                        <img src="{{$slider->getImage()}}" alt="slider">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <?php elseif($count == 2): ?>
                         <div class="swiper-slide bg-2 main-slider-bg-light">
                             <div class="container table">
                                 <div class="row table-cell">
                                     <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                                         <div class="slider-content align-both">
                                             <h2 class="slider-content-title" data-swiper-parallax="-100">
                                                 <span class="c-primary">{{$slider->title}}</span>
                                             </h2>
                                             <h6 class="slider-content-text" data-swiper-parallax="-200">{!! $slider->content !!}</h6>
                                             <div class="main-slider-btn-wrap" data-swiper-parallax="-300">

                                                 @if($slider->btn)
                                                     <div class="main-slider-btn-wrap" data-swiper-parallax="-300">
                                                         <a href="{{$slider->btn_links}}" class="btn btn-border btn--with-shadow c-primary">
                                                             {{$slider->btn}}
                                                         </a>
                                                     </div>
                                                 @endif

                                             </div>

                                         </div>
                                     </div>

                                 </div>
                             </div>
                         </div>
                    <?php elseif($count == 3): ?>
                         <div class="swiper-slide thumb-left bg-3 main-slider-bg-light">
                             <div class="container table full-height">
                                 <div class="row table-cell">
                                     <div class="col-lg-6 col-sm-12 table-cell">
                                         <div class="slider-content align-both">
                                             <h2 class="slider-content-title" data-swiper-parallax="-100">
                                                 {{$slider->title}}</h2>
                                             <h6 class="slider-content-text" data-swiper-parallax="-200">{!! $slider->content !!}
                                             </h6>
                                             <div class="main-slider-btn-wrap" data-swiper-parallax="-300">
                                                 @if($slider->btn)
                                                     <a href="{{$slider->btn_links}}" class="btn btn--lime btn--with-shadow">
                                                         {{$slider->btn}}
                                                     </a>
                                                 @endif
                                             </div>
                                         </div>
                                     </div>
                                     <div class="col-lg-6 col-sm-12 table-cell">
                                         <div class="slider-thumb" data-swiper-parallax="-300" data-swiper-parallax-duration="500">
                                             <img src="{{$slider->getImage()}}" alt="slider">
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                    <?php endif; ?>
                @endforeach


            </div>
            <div class="btn-prev with-bg">
                <svg class="utouch-icon icon-hover utouch-icon-arrow-left-1"><use xlink:href="#utouch-icon-arrow-left-1"></use></svg>
                <svg class="utouch-icon utouch-icon-arrow-left1"><use xlink:href="#utouch-icon-arrow-left1"></use></svg>
            </div>
            <div class="btn-next with-bg">
                <svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
                <svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
            </div>
        </div>
    </div>
    @endif

    <!-- Info Boxes -->
    <section class="medium-padding100">
        <div class="container">
            <div class="row">
                @foreach($services as $service)
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="crumina-module crumina-info-box info-box--standard-hover">
                        <div class="info-box-image">
                            <img class="utouch-icon" src="{{$service->getImage()}}" alt="smartphone">
                            <img class="cloud" src="{{asset('front/img/clouds8.png')}}" alt="cloud">
                        </div>
                        <div class="info-box-content">
                            <a href="{{route('service.show', $service->slug)}}" class="h5 info-box-title">{{$service->title}}</a>
                            <p class="info-box-text">{!! $service->desc !!}
                            </p>
                        </div>
                        <a href="{{route('service.show', $service->slug)}}" class="btn-next">
                            <svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
                            <svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- ... end Info Boxes -->

    @if($frames)
    <section class="crumina-module crumina-clients background-contain bg-yellow">
        <div class="container">
            <div class="row">
                @foreach($frames as $frame)
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <a class="clients-item">
                        <span class="clients-images">
                            <img src="{{$frame->getImage()}}" class=""style="width: 100px" alt="{{$frame->title}}">
                            <img src="{{$frame->getImage()}}" class="hover" alt="{{$frame->title}}">
                        </span>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    @endif

    <!-- Info Boxes -->
    <section class="bg-9 background-contain medium-padding120">
        <div class="container">
            <div class="row">
                <div class="display-flex info-boxes">
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                        <div class="crumina-module crumina-info-box info-box--standard-round icon-right negative-margin-right150">
                            <div class="info-box-image">
                                <img src="svg-icons/chat.svg" alt="chat" class="utouch-icon">
                            </div>
                            <div class="info-box-content">
                                <h5 class="info-box-title">Private Chat Integration</h5>
                                <p class="info-box-text">Sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                                    tincidunt.
                                </p>
                            </div>
                        </div>

                        <div class="crumina-module crumina-info-box info-box--standard-round icon-right negative-margin-right150">
                            <div class="info-box-image">
                                <img src="svg-icons/pictures.svg" alt="chat" class="utouch-icon">
                            </div>
                            <div class="info-box-content">
                                <h5 class="info-box-title">Perfect Grafic View</h5>
                                <p class="info-box-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
                                    nonummy nibh euismod.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 align-center">
                        <img class="particular-image" src="{{asset('front/img/image.png')}}" alt="image">
                        <a href="03_products.html" class="btn btn--red btn--with-shadow">
                            Learn More
                        </a>
                    </div>

                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                        <div class="crumina-module crumina-info-box info-box--standard-round negative-margin-left150">
                            <div class="info-box-image">
                                <img src="svg-icons/clock.svg" alt="chat" class="utouch-icon">
                            </div>
                            <div class="info-box-content">
                                <h5 class="info-box-title">Lifetime Updates</h5>
                                <p class="info-box-text">Sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                                    tincidunt.
                                </p>
                            </div>
                        </div>

                        <div class="crumina-module crumina-info-box info-box--standard-round negative-margin-left150">
                            <div class="info-box-image">
                                <img src="svg-icons/calendar.svg" alt="chat" class="utouch-icon">
                            </div>
                            <div class="info-box-content">
                                <h5 class="info-box-title">Calendar Sinhronize</h5>
                                <p class="info-box-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
                                    nonummy euismod.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ... Info Boxes -->

    <!-- Info Boxes -->
    <section class="crumina-module crumina-module-slider bg-blue-lighteen background-contain bg-11 medium-padding100">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="crumina-module crumina-heading">
                        <h6 class="heading-sup-title">Screenshots</h6>
                        <h2 class="heading-title">Beautiful interface</h2>
                        <p class="heading-text">Claritas est etiam processus dynamicus, qui sequitur mutationem
                            consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram,
                            anteposuerit litterarum formas humanitatis per est usus legentis in iis qui facit eorum
                            claritatem.
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="crumina-module crumina-info-box info-box--standard">
                                <div class="info-box-image display-flex">
                                    <svg class="utouch-icon utouch-icon-checked"><use xlink:href="#utouch-icon-checked"></use></svg>
                                    <h6 class="info-box-title">Quick Settings</h6>
                                </div>
                                <p class="info-box-text">Wisi enim ad minim veniam, quis nostrud exerci tation qui
                                    nunc nobis videntur parum clari.
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="crumina-module crumina-info-box info-box--standard">
                                <div class="info-box-image display-flex">
                                    <svg class="utouch-icon utouch-icon-checked"><use xlink:href="#utouch-icon-checked"></use></svg>
                                    <h6 class="info-box-title">Looks Perfect</h6>
                                </div>
                                <p class="info-box-text">Sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-5 col-lg-offset-1 col-md-12 col-md-offset-0 col-sm-12 col-xs-12">
                    <div class="swiper-container pagination-bottom slider-tripple-right-image" data-show-items="1" data-effect="coverflow" data-centered-slider="false" data-stretch="170" data-depth="195">
                        <div class="swiper-wrapper">
                            @foreach($works as $frame)
                            <div class="swiper-slide">
                                <img src="{{$frame->getImage()}}" alt="slide">
                            </div>
                            @endforeach

                        </div>
                        <!-- If we need pagination -->
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ... end Info Boxes -->
    <!-- Subscribe Form -->
    <section class="bg-primary-color background-contain bg-14 crumina-module crumina-module-subscribe-form">
        <div class="container">
            <div class="row">
                <div class="subscribe-form">
                    <div class="subscribe-main-content">
                        <img class="subscribe-img" src="{{asset('front/img/subscribe-img.png')}}" alt="image">

                        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                            <div class="crumina-module crumina-heading">
                                <h2 class="heading-title">Love offers and discounts? Subscribe and save.</h2>
                                <p class="heading-text">Claritas est etiam processus dynamicus, qui sequitur mutationem
                                    consuetudium lectorum putamus claram.
                                </p>
                            </div>

                            <form class="form-inline subscribe-form-js" method="post" action="import.php">
                                <input name="email" placeholder="Enter your email address" type="email">
                                <button class="btn btn--green-light">
                                    Subscribe
                                </button>
                            </form>
                        </div>

                    </div>
                    <div class="subscribe-layer"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Subscribe Form -->
@endsection