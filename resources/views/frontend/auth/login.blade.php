@extends('frontend.layout')
@section('content')
    <div class="container">
        <div class="contact-form medium-padding120">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="heading">
                        <h4 class="heading-title">Авторизация</h4>
                        <div class="heading-line">
                            <span class="short-line"></span>
                            <span class="long-line"></span>
                        </div>
                        @if(session('status'))
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="alert alert-success">
                                            {{session('status')}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @include('backend.message.errors')
                        <p class="heading-text">Если еще не зарегистрированы, то Вам <a href="/register">сюда</a>.</p>
                    </div>
                </div>
            </div>

            <form class="contact-form loginform" method="post" action="/login">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <input name="email" class="email input-standard-grey" placeholder="Ваш email" value="{{old('email')}}" type="email" required="">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <input name="password" class="email input-standard-grey" placeholder="Ваш пароль" type="text" required="">
                    </div>

                </div>
                <div class="row">
                    <div class="submit-block table">
                        <div class="col-lg-3 table-cell">
                            {{--<button  type="submit"  class="btn btn-small btn--primary">--}}
                                {{--<span class="text">Submit Now</span>--}}
                            {{--</button>--}}
                            <button type="submit" class="btn send-btn">Вход</button>
                        </div>
                        <div class="col-lg-5 table-cell">
                            <div class="submit-block-text">
                                Please, let us know any particular things to check and the best time
                                to contact you by phone (if provided).
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

