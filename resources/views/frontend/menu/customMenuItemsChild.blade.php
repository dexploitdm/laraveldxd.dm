@foreach($items as $item)
    <li {{ (URL::current() == $item->url()) ? "class=active" : '' }}>
        <a href="{{ $item->url() }}">{{ $item->title }}<i class="seoicon-right-arrow"></i></a>
        @if($item->hasChildren())
            <ul class="dropdown">
                @include('frontend.menu.customMenuItems', ['items'=>$item->children()])
            </ul>
        @endif
@endforeach