<?php

use App\User;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/users', function () {
    return User::all();
});
Route::get('/tests', function () {
    return \App\Slider::all();
});
Route::get('/works', function () {
    return \App\Service::all();
   // $works = \App\Service::all();
    //$tests = \App\Slider::all();

    //return response()->json( $works);
//    return  view()->share([
//       // 'tests' => $tests,
//        'works'=> $works,
//    ]);

});