<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('/{any}', 'SpaController@index')->where('any', '.*');
Route::get('/works', 'SpaController@index')->where('any', '.*')->name('home');

//
//Route::get('/','Frontend\HomeController@index')->name('home');
//Route::get('/blog','Frontend\BlogController@index')->name('blog.index');
//Route::get('/blog/{slug}','Frontend\BlogController@show')->name('blog.show');
//Route::get('/works','Frontend\WorksController@index')->name('work.index');
//Route::get('/works/{slug}','Frontend\WorksController@show')->name('work.show');
//Route::get('/works/like/{id}', 'Frontend\WorksController@like');
//Route::get('/service','Frontend\ServicesController@index')->name('service.index');
//Route::get('/service/{slug}','Frontend\ServicesController@show')->name('service.show');
//Route::get('/about','Frontend\AboutController@index')->name('about');
//
//Route::get('/tag/{slug}','Frontend\BlogController@tag')->name('tag.show');
//Route::get('/category/{slug}','Frontend\BlogController@category')->name('category.show');
//Route::post('/subscribe', 'Frontend\SubsController@subscribe');
//Route::post('/order', 'Frontend\MailController@order');
//Route::get('/verify/{token}', 'Frontend\SubsController@verify');
//Route::get('searchSimple', 'SearchController@index')->name('searchSimple');


Route::group(['prefix'=>'backend', 'namespace'=>'Backend','middleware'	=>	'admin'],function(){
    Route::get('/','DashboardController@index')->name('backend');
    Route::resource('/categories','CategoriesController');
    Route::resource('/tags','TagsController');
    Route::resource('/posts','PostsController');
    Route::resource('/services','ServicesController');
    Route::resource('/works_up','WorksController');
    Route::resource('/filters','FiltersController');
    Route::resource('/users','UsersController');
    Route::get('/comments', 'CommentsController@index')->name('comments');
    Route::get('/comments/toggle/{id}', 'CommentsController@toggle');
    Route::delete('/comments/{id}/destroy', 'CommentsController@destroy')->name('comments.destroy');
    Route::resource('/subscribers', 'SubsController');
    Route::resource('/sliders','SliderController');
    Route::resource('/menus','MenusController');
    Route::resource('/abouts','AboutController');
    Route::resource('/absliders','AbsSliderController');
    Route::resource('/commands','CommandsController');
    Route::resource('/frames','FrameController');
    Route::resource('/settings','SettingsController');
});

Route::group(['middleware' => 'guest'], function (){
    Route::get('/register' , 'Auth\AuthController@registerForm');
    Route::post('/register' , 'Auth\AuthController@register');
    Route::get('/login' , 'Auth\AuthController@loginForm')->name('login');
    Route::post('/login', 'Auth\AuthController@login');
});
Route::group(['middleware' => 'auth'], function (){
    Route::get('/logout' , 'Auth\AuthController@logout')->name('logout');
    Route::get('/profile', 'Auth\ProfileController@index');
    Route::post('/profile', 'Auth\ProfileController@store');
    Route::post('/comment' , 'Frontend\CommentsController@store');
});