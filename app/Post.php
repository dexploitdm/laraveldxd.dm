<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title','content','image','date','status','is_featured','keywords'];
    use Sluggable;
    public function sluggable()
    {
        // TODO: Implement sluggable() method.
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function tags(){
        return $this->belongsToMany(
            Tag::class,
            'posts_tags',
            'post_id',
            'tag_id');
    }
    public function author(){
        return $this->belongsTo(User::class, 'user_id');
    }
    public function comments(){
        return $this->hasMany(Comment::class);
    }
    //Добавление
    public static function add($fields){
        $post = new static();
        $post->fill($fields);
        $post->user_id = Auth::user()->id;
        $post->save();
        return $post;
    }
    //Редактирование
    public function edit($fields) {
        $this->fill($fields);
        $this->save();
    }
    //Удаление
    public function remove() {
        $this->removeImage();
        $this->delete();
    }
    public function removeImage(){
        if($this->image != null){
            Storage::delete('uploads/' . $this->image);
        }
    }
    //Изображение
    public function uploadImage($image) {
        if($image == null){ return; }
        $this->removeImage();
        $filename =str_random(10) . '.' . $image->extension();
        $image->storeAs('uploads',$filename);
        $this->image = $filename;
        $this->save();
    }
    public function getImage() {
        if($this->image == null) {
            return 'img/no-image.png';
        }
        return '/uploads/' . $this->image;
    }
    //Категории
    public function setCategory($id) {
        if($id == null ){ return; }
        $this->category_id = $id;
        $this->save();
    }
    //Выбор категории
    public function getCategoryTitle() {
        if($this->category != null ) {
            return $this->category->title;
        }
        return 'Нет категории';
    }
    public function getCategoryID() {
        return $this->category != null ? $this->category->id : null;
    }
    public function hasCategory() {
        return $this->category != null ? true : false;
    }
    //Теги
    public function setTags($ids) {
        if($ids == null ){ return; }
        $this->tags()->sync($ids);
    }
    //Вывод тегов
    public function getTagsTitles() {
        return (!$this->tags->isEmpty()) ? implode('.', $this->tags->pluck('title')->all()) : 'Нет тегов';
    }
    //Статус
    public function toggleStatus($value) {
        if($value == null ){
            return $this->setDraft();
        } else {
            return $this->setPublic();
        }
    }
    public function setDraft() {
        $this->status = 0;
        $this->save();
    }
    public function setPublic() {
        $this->status = 1;
        $this->save();
    }
    //Рекомендованые
    public function toggleFeatured($value) {
        if($value == null ){
            return $this->setStandart();
        } else {
            return $this->setFeatured();
        }
    }
    public function setStandart() {
        $this->is_featured = 0;
        $this->save();
    }
    public function setFeatured() {
        $this->is_featured = 1;
        $this->save();
    }


    /*
     * Второсотепенные методы
     */
    //Дата
    public function setDateAttribute($value){
        $date = Carbon::createFromFormat('d/m/y', $value)->format('Y-m-d');
        $this->attributes['date'] = $date;
    }
    public function getDateAttribute($value){
        $date = Carbon::createFromFormat('Y-m-d', $value)->format('d/m/y');
        return $date;
    }
    public function getDate()
    {
        return Carbon::createFromFormat('d/m/y', $this->date)->format('F d, Y');
    }
    public function related() {
        return self::all()->except($this->id);
    }
    public function getPopularPost(){
        return self::orderBy('views','desc')->take(3)-get();
    }
    public function getComments() {

        return $this->comments()->where('status',1)->get();
    }
    public function hasPrevious() {
        return self::where('id','<', $this->id)->max('id');
    }
    public function getPrevious() {
        $post_ID = $this->hasPrevious();
        return self::find($post_ID);
    }
    public function hasNext() {
        return self::where('id','>', $this->id)->min('id');
    }
    public function getNext() {
        $post_ID = $this->hasNext();
        return self::find($post_ID);
    }

}
