<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    protected $fillable = ['title'];

    use Sluggable;
    public function sluggable()
    {
        // TODO: Implement sluggable() method.
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
    public function works() {

        return $this->hasMany(Work::class);
    }
}
