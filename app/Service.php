<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Service extends Model
{
    protected $fillable = ['title','content','image','desc','keywords','background'];
    use Sluggable;
    public function sluggable()
    {
        // TODO: Implement sluggable() method.
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
    //Добавление
    public static function add($fields){
        $post = new static();
        $post->fill($fields);
        $post->save();
        return $post;
    }
    //Редактирование
    public function edit($fields) {
        $this->fill($fields);
        $this->save();
    }
    //Удаление
    public function remove() {
        $this->removeImage();
        $this->delete();
    }
    public function removeImage(){
        if($this->image != null){
            Storage::delete('uploads/services' . $this->image);
        }
    }
    //Изображение
    public function uploadImage($image) {
        if($image == null){ return; }
        $this->removeImage();
        $filename =str_random(10) . '.' . $image->extension();
        $image->storeAs('uploads/services',$filename);
        $this->image = $filename;
        $this->save();
    }
    public function getImage() {
        if($this->image == null) {
            return 'img/no-image.png';
        }
        return '/uploads/services/' . $this->image;
    }
}
