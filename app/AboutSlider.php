<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class AboutSlider extends Model
{
    protected $fillable = ['title','btn_name','btn_link','content','image'];

    //Добавление
    public static function add($fields){
        $abslider = new static();
        $abslider->fill($fields);
        $abslider->save();
        return $abslider;
    }
    //Редактирование
    public function edit($fields) {
        $this->fill($fields);
        $this->save();
    }
    //Удаление
    public function remove() {
        $this->removeImage();
        $this->delete();
    }
    public function removeImage(){
        if($this->image != null){
            Storage::delete('uploads/abouts' . $this->image);
        }
    }
    //Изображение
    public function uploadImage($image) {
        if($image == null){ return; }
        $this->removeImage();
        $filename =str_random(10) . '.' . $image->extension();
        $image->storeAs('uploads/abouts',$filename);
        $this->image = $filename;
        $this->save();
    }
    public function getImage() {
        if($this->image == null) {
            return 'img/no-image.png';
        }
        return '/uploads/abouts/' . $this->image;
    }
}
