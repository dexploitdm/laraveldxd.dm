<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = ['title','path','parent'];

    public static function addMenu($request) {
        $menu = new static();
        $data = $request->only('type','title','parent');
        if(empty($data)) {
            return ['error' => 'Нет данных'];
        }
        switch($data['type']) {
            case 'customLink':
                $data['path'] = $request->input('custom_link');
                break;
            case 'blogLink':
                if($request->input('category_alias')) {
                    if($request->input('category_alias') == 'parent') {
                        $data['path'] = route('blog.index');
                    }
                    else {
                        $data['path'] = route('category.show',['cat_alias' => $request->input('category_alias')]);
                    }
                }
                else if($request->input('blog_alias')) {
                    $data['path'] = route('blog.show',['slug' =>$request->input('blog_alias')]);
                }
                break;
        }
        unset($data['type']);

        $menu->fill($data);
        $menu->save();
        return $menu;
    }
}
