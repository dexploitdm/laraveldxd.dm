<?php

namespace App;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Work extends Model
{
    protected $fillable = ['title','content','image',
        'desc','keywords','image_two','image_three','image_blocks','text_block','date',
        'links','github','filter_id','technology','is_slider','likes','blocks_image'
    ];
    use Sluggable;
    public function sluggable()
    {
        // TODO: Implement sluggable() method.
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
    public function filter() {
        return $this->belongsTo(Filter::class);
    }
    //Добавление
    public static function add($fields){
        $post = new static();
        $post->fill($fields);
        $post->save();
        return $post;
    }
    //Редактирование
    public function edit($fields) {
        $this->fill($fields);
        $this->save();
    }
    //Удаление
    public function remove() {
        $this->removeImage();
        $this->delete();
    }
    public function removeImage(){
        if($this->image != null){
            Storage::delete('uploads/works' . $this->image);
        }
        if($this->image_two != null){
            Storage::delete('uploads/works' . $this->image_two);
        }
        if($this->image_three != null){
            Storage::delete('uploads/works' . $this->image_three);
        }
        if($this->image_blocks != null){
            Storage::delete('uploads/works' . $this->image_blocks);
        }
    }
    //Изображение 1
    public function uploadImage($image) {
        if($image == null){ return; }
        $this->removeImage();
        $filename =str_random(10) . '.' . $image->extension();
        $image->storeAs('uploads/works',$filename);
        $this->image = $filename;
        $this->save();
    }
    //Изображение 2
    public function uploadImageTwo($image_two) {
        if($image_two == null){ return; }
        $this->removeImage();
        $filename =str_random(10) . '.' . $image_two->extension();
        $image_two->storeAs('uploads/works',$filename);
        $this->image_two = $filename;
        $this->save();
    }
    //Изображение 3
    public function uploadImageThree($image_three) {
        if($image_three == null){ return; }
        $this->removeImage();
        $filename =str_random(10) . '.' . $image_three->extension();
        $image_three->storeAs('uploads/works',$filename);
        $this->image_three = $filename;
        $this->save();
    }
    //Изображение 4
    public function uploadImageBlocks($image_blocks) {
        if($image_blocks == null){ return; }
        $this->removeImage();
        $filename =str_random(10) . '.' . $image_blocks->extension();
        $image_blocks->storeAs('uploads/works',$filename);
        $this->image_blocks = $filename;
        $this->save();
    }
    public function getImage() {
        if($this->image == null) {
            return 'img/no-image.png';
        }
        return '/uploads/works/' . $this->image;
    }
    public function getImageTwo() {
        if($this->image_two == null) {
            return 'img/no-image.png';
        }
        return '/uploads/works/' . $this->image_two;
    }
    public function getImageThree() {
        if($this->image_three == null) {
            return 'img/no-image.png';
        }
        return '/uploads/works/' . $this->image_three;
    }
    public function getImageBlocks() {
        if($this->image_blocks == null) {
            return 'img/no-image.png';
        }
        return '/uploads/works/' . $this->image_blocks;
    }




    //Вид слайдера или обычный
    public function toggleView($value) {
        if($value == null ){
            return $this->setStandart();
        } else {
            return $this->setSlider();
        }
    }
    public function setStandart() {
        $this->is_slider = 0;
        $this->save();
    }
    public function setSlider() {
        $this->is_slider = 1;
        $this->save();
    }
    //Показать изображения в виде блоков (2 img)
    public function toggleImgBlocks($value) {
        if($value == null ){
            return $this->setOther();
        } else {
            return $this->setImgBlocks();
        }
    }
    public function setOther() {
        $this->blocks_image = 0;
        $this->save();
    }
    public function setImgBlocks() {
        $this->blocks_image = 1;
        $this->save();
    }
    //Дата
    public function setDateAttribute($value){
        $date = Carbon::createFromFormat('d/m/y', $value)->format('Y-m-d');
        $this->attributes['date'] = $date;
    }
    public function getDateAttribute($value){
        $date = Carbon::createFromFormat('Y-m-d', $value)->format('d/m/y');
        return $date;
    }
    public function getDate()
    {
        return Carbon::createFromFormat('d/m/y', $this->date)->format('F d, Y');
    }

    //Фильтры
    public function setFilter($id) {
        if($id == null ){ return; }
        $this->filter_id = $id;
        $this->save();
    }
    public function getFilterTitle() {
        if($this->filter != null ) {
            return $this->filter->title;
        }
        return 'Нет фильтра';
    }
    public function getFilterID() {
        return $this->filter != null ? $this->filter->id : null;
    }
    public function hasCFilter() {
        return $this->filter != null ? true : false;
    }
    public function plusLike(){

        $this->save();
    }
}
