<?php

namespace App\Providers;

use App\Category;
use App\Comment;
use App\Menu;
use App\Post;
use App\Service;
use App\Subscription;
use App\Tag;
use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Menu as LavMenu;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        view()->composer('backend.navigate', function($view){
            $view->with('takeAdminSetting', User::orderBy('status', '1')->take(1)->get());
            $view->with('newCommentsCount', Comment::where('status',0)->count());
            $view->with('subsCount', Subscription::all()->count());
        });
        view()->composer('frontend.widgets.sidebar', function($view){
            //$view->with('takeAdminSetting', User::orderBy('status', '1')->take(1)->get());
            $view->with('tagsCloud', Tag::all());
            $view->with('recentPosts', Post::orderBy('date', 'desc')->take(3)->get());
            $view->with('CategoriesCloud', Category::has('posts')->get());
        });
        view()->composer('frontend.layout', function($view){
            $arrMenu = Menu::all();
            $menu = $this->buildMenu($arrMenu);
            $view->with('menu', $menu);
            $view->with('servicesNav', Service::orderBy('created_at', 'desc')->take(4)->get());
        });
    }
    public function buildMenu ($arrMenu){
        $mBuilder = LavMenu::make('MyNav', function($m) use ($arrMenu){
            foreach($arrMenu as $item){
                /*
                 * Для родительского пункта меню формируем элемент меню в корне
                 * и с помощью метода id присваиваем каждому пункту идентификатор
                 */
                if($item->parent == 0){
                    $m->add($item->title, $item->path)->id($item->id);
                }
                //иначе формируем дочерний пункт меню
                else {
                    //ищем для текущего дочернего пункта меню в объекте меню ($m)
                    //id родительского пункта (из БД)
                    if($m->find($item->parent)){
                        $m->find($item->parent)->add($item->title, $item->path)->id($item->id);
                    }
                }
            }
        });
        return $mBuilder;
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
