<?php

namespace App\Http\Controllers\Backend;

use App\Framework;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FrameController extends Controller
{
    public function index() {
        $frames = Framework::all();
        return view('backend.frames.index',[
            'frames' => $frames
        ]);
    }
    public function create(){
        return view('backend.frames.create');
    }
    public function store(Request $request){
        $this->validate($request,[
            'title' => 'required',
            'image' => 'nullable|image'
        ]);
        $frame = Framework::add($request->all());
        $frame->uploadImage($request->file('image'));
        return redirect()->route('frames.index')->with('message','Инструмент успешно добавлен');
    }
    public function edit($id){
        $frame = Framework::find($id);
        return view('backend.frames.edit',[
            'frame' => $frame
        ]);
    }
    public function update(Request $request, $id) {
        $this->validate($request,[
            'title' => 'required',
            'image' => 'nullable|image'
        ]);
        $frame = Framework::find($id);
        $frame->update($request->all());
        $frame->uploadImage($request->file('image'));
        return redirect()->route('frames.index')->with('message','Инструмент успешно обновлен');
    }

    public function destroy($id){
        Framework::find($id)->remove();
        return redirect()->route('frames.index')->with('message','Инструмент успешно удален');
    }
}
