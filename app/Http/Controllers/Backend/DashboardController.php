<?php

namespace App\Http\Controllers\Backend;

use Alexusmai\YandexMetrika\YandexMetrika;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index(){
        return view('backend.home.index');
    }

}
