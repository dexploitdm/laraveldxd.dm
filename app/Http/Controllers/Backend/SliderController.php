<?php

namespace App\Http\Controllers\Backend;

use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{
    public function index() {
        $sliders = Slider::all();
        return view('backend.sliders.index',[
            'sliders' => $sliders
        ]);
    }
    public function create(){
        return view('backend.sliders.create');
    }
    public function store(Request $request){
        $this->validate($request,[
            'title' => 'required',
            'background' => 'required',
            'content' => 'required',
            'image' => 'nullable|image'
        ]);
        $slider = Slider::add($request->all());
        $slider->uploadImage($request->file('image'));
        return redirect()->route('sliders.index')->with('message','Слайдер успешно добавлена');
    }
    public function edit($id){
        $slider = Slider::find($id);
        return view('backend.sliders.edit',[
            'slider' => $slider
        ]);
    }
    public function update(Request $request, $id) {
        $this->validate($request,[
            'title' => 'required',
            'background' => 'required',
            'content' => 'required',
            'image' => 'nullable|image'
        ]);
        $slider = Slider::find($id);
        $slider->update($request->all());
        $slider->uploadImage($request->file('image'));
        return redirect()->route('sliders.index')->with('message','Слайдер успешно обновлен');
    }

    public function destroy($id){
        Slider::find($id)->remove();
        return redirect()->route('sliders.index')->with('message','Слайдер успешно удален');
    }


}
