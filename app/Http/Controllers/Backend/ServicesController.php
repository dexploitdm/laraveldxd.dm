<?php

namespace App\Http\Controllers\Backend;

use App\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServicesController extends Controller
{
    public function index() {
        $services = Service::all();
        return view('backend.services.index',[
            'services' => $services
        ]);
    }
    public function create() {
        return view('backend.services.create');
    }
    public function store(Request $request){
        $this->validate($request,[
            'title' => 'required',
            'background' => 'required',
            'content' => 'required',
            'desc' => 'required',
            'image' => 'nullable|image'
        ]);
        $post = Service::add($request->all());
        $post->uploadImage($request->file('image'));
        return redirect()->route('services.index')->with('message','Услуга успешно добавлена');
    }
    public function edit($id) {
        $service = Service::find($id);
        return view('backend.services.edit',[
            'service' => $service
        ]);
    }
    public function update(Request $request, $id){
        $this->validate($request,[
            'title' => 'required',
            'background' => 'required',
            'content' => 'required',
            'image' => 'nullable|image'
        ]);
        $post = Service::find($id);
        $post->edit($request->all());
        $post->uploadImage($request->file('image'));
        return redirect()->route('services.index')->with('message','Услуга успешно обновлена');
    }
    public function destroy($id){
        Service::find($id)->remove();
        return redirect()->route('services.index')->with('message','Услуга успешно удалена');
    }
}
