<?php

namespace App\Http\Controllers\Backend;

use App\About;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    public function index() {
        $abouts = About::all();
        return view('backend.abouts.index',[
            'abouts' => $abouts
        ]);
    }
    public function create() {
        return view('backend.abouts.create');
    }
    public function store(Request $request){
        $this->validate($request,[
            'title' => 'required',
            'content' => 'required',
            'image' => 'nullable|image'
        ]);
        About::create($request->all());
        return redirect()->route('abouts.index')->with('message','Страница успешно добавлена');
    }
    public function edit($id) {
        $about = About::find($id);
        return view('backend.abouts.edit',[
            'about' => $about
        ]);
    }
    public function update(Request $request, $id){
        $this->validate($request,[
            'title' => 'required',
            'content' => 'required',
            'image' => 'nullable|image'
        ]);
        $about = About::find($id);
        $about->update($request->all());
        return redirect()->route('abouts.index')->with('message','Страница успешно обновлена');
    }
    public function destroy($id){
        About::find($id)->delete();
        return redirect()->route('abouts.index')->with('message','Страница успешно удалена');
    }
}
