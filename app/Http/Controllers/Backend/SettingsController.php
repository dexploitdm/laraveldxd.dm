<?php

namespace App\Http\Controllers\Backend;

use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    public function index() {
        $settings = Setting::all();
        $countsettings = Setting::all()->count();
        return view('backend.settings.index',[
            'settings' => $settings,
            'countsettings' => $countsettings
        ]);
    }
    public function create(){
        return view('backend.settings.create');
    }
    public function store(Request $request){
        $this->validate($request,[
            'title' => 'required',
            'logo' => 'nullable|image',
            'banner' => 'nullable|image',
        ]);
        $setting = Setting::add($request->all());
        $setting->uploadImage($request->file('logo'));
        $setting->uploadBanner($request->file('banner'));
        return redirect()->route('settings.index')->with('message','Настройки успешно добавлены');
    }
    public function edit($id){
        $setting = Setting::find($id);
        return view('backend.settings.edit',[
            'setting' => $setting
        ]);
    }
    public function update(Request $request, $id) {
        $this->validate($request,[
            'title' => 'required',
            'logo' => 'nullable|image',
            'banner' => 'nullable|image',
        ]);
        $setting = Setting::find($id);
        $setting->update($request->all());
        $setting->uploadImage($request->file('logo'));
        $setting->uploadBanner($request->file('banner'));
        return redirect()->route('settings.index')->with('message','Настройки успешно обновлены');
    }

    public function destroy($id){
        Setting::find($id)->remove();
        return redirect()->route('settings.index')->with('message','Настройки успешно удалены');
    }
}
