<?php

namespace App\Http\Controllers\Backend;

use App\Commands;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommandsController extends Controller
{
    public function index() {
        $commands = Commands::all();
        return view('backend.commands.index',[
            'commands' => $commands
        ]);
    }
    public function create(){
        return view('backend.commands.create');
    }
    public function store(Request $request){
        $this->validate($request,[
            'title' => 'required',
            'image' => 'nullable|image'
        ]);
        $command = Commands::add($request->all());
        $command->uploadImage($request->file('image'));
        return redirect()->route('commands.index')->with('message','Сотрудник успешно добавлен');
    }
    public function edit($id){
        $command = Commands::find($id);
        return view('backend.commands.edit',[
            'command' => $command
        ]);
    }
    public function update(Request $request, $id) {
        $this->validate($request,[
            'title' => 'required',
            'image' => 'nullable|image'
        ]);
        $command = Commands::find($id);
        $command->update($request->all());
        $command->uploadImage($request->file('image'));
        return redirect()->route('commands.index')->with('message','Сотрудник успешно обновлен');
    }

    public function destroy($id){
        Commands::find($id)->remove();
        return redirect()->route('commands.index')->with('message','Сотрудник успешно удален');
    }
}
