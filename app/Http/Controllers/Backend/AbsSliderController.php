<?php

namespace App\Http\Controllers\Backend;

use App\AboutSlider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AbsSliderController extends Controller
{
    public function index() {
        $absliders = AboutSlider::all();
        return view('backend.absliders.index',[
            'absliders' => $absliders
        ]);
    }
    public function create(){
        return view('backend.absliders.create');
    }
    public function store(Request $request){
        $this->validate($request,[
            'title' => 'required',
            'content' => 'required',
            'image' => 'nullable|image'
        ]);
        $abslider = AboutSlider::add($request->all());
        $abslider->uploadImage($request->file('image'));
        return redirect()->route('absliders.index')->with('message','Слайдер успешно добавлена');
    }
    public function edit($id){
        $abslider = AboutSlider::find($id);
        return view('backend.absliders.edit',[
            'abslider' => $abslider
        ]);
    }
    public function update(Request $request, $id) {
        $this->validate($request,[
            'title' => 'required',
            'content' => 'required',
            'image' => 'nullable|image'
        ]);
        $abslider = AboutSlider::find($id);
        $abslider->update($request->all());
        $abslider->uploadImage($request->file('image'));
        return redirect()->route('absliders.index')->with('message','Слайдер успешно обновлен');
    }

    public function destroy($id){
        AboutSlider::find($id)->remove();
        return redirect()->route('absliders.index')->with('message','Слайдер успешно удален');
    }
}
