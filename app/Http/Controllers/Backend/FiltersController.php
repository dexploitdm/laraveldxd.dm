<?php

namespace App\Http\Controllers\Backend;

use App\Filter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class FiltersController extends Controller
{
    public function index(){

        $filters = DB::table('filters')->orderBy('created_at','desc')->paginate(7);
        return view('backend.filters.index',
            ['filters' => $filters]
        );
    }
    public function create() {
        return view('backend.filters.create');
    }
    public function store(Request $request) {
        $this->validate($request, [
            'title' => 'required'
        ]);
        Filter::create($request->all());
        return redirect()->route('filters.index')->with('message','Фильтер успешно добавлен');
    }
    public function edit($id){
        $filter = Filter::find($id);
        return view('backend.filters.edit',[
            'filter'=>$filter
        ]);
    }
    public function update(Request $request, $id){
        $this->validate($request,[
            'title' => 'required'
        ]);
        $filter = Filter::find($id);
        $filter->update($request->all());
        return redirect()->route('filters.index')->with('message','Фильтер успешно обновлен');
    }
    public function destroy($id){
        Filter::find($id)->delete();
        return redirect()->route('filters.index')->with('message','Фильтер успешно удален');
    }
}
