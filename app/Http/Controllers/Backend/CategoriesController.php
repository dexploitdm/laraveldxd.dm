<?php

namespace App\Http\Controllers\Backend;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CategoriesController extends Controller
{
    public function index(){

        $categories = DB::table('categories')->orderBy('created_at','desc')->paginate(7);
        return view('backend.categories.index',
            ['categories' => $categories]
        );
    }
    public function create() {
        return view('backend.categories.create');
    }
    public function store(Request $request) {
        $this->validate($request, [
           'title' => 'required'
        ]);
        Category::create($request->all());
        return redirect()->route('categories.index')->with('message','Категория успешно добавлена');
    }
    public function edit($id){
        $category = Category::find($id);
        return view('backend.categories.edit',[
            'category'=>$category
        ]);
    }
    public function update(Request $request, $id){
        $this->validate($request,[
            'title' => 'required'
        ]);
        $category = Category::find($id);
        $category->update($request->all());
        return redirect()->route('categories.index')->with('message','Категория успешно обновлена');
    }
    public function destroy($id){
        Category::find($id)->delete();
        return redirect()->route('categories.index')->with('message','Категория успешно удалена');
    }
}
