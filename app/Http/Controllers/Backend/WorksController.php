<?php

namespace App\Http\Controllers\Backend;

use App\Filter;
use App\Work;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WorksController extends Controller
{
    public function index() {
        $works = Work::orderBy('created_at','desc')->paginate(7);
        return view('backend.works_up.index', ['works' => $works]);
    }
    public function create(){
        $filters = Filter::pluck('title','id')->all();
        return view('backend.works_up.create',[
            'filters' => $filters
        ]);
    }
    public function store(Request $request){
        $this->validate($request,[
            'title' => 'required',
            'content' => 'required',
            'date' => 'required',
            'links' => 'required',
            'image' => 'nullable|image',
            'image_two' => 'nullable|image',
            'image_three' => 'nullable|image',
            'image_blocks' => 'nullable|image',
        ]);
        $works = Work::add($request->all());
        $works->uploadImage($request->file('image'));
        $works->uploadImageTwo($request->file('image_two'));
        $works->uploadImageThree($request->file('image_three'));
        $works->uploadImageBlocks($request->file('image_blocks'));
        $works->toggleView($request->get('is_slider'));
        $works->toggleImgBlocks($request->get('blocks_image'));
        return redirect()->route('works_up.index')->with('message','Проект успешно добавлен');
    }

    public function edit($id){
        $work = Work::find($id);
        $filters = Filter::pluck('title','id')->all();
        return view('backend.works_up.edit',[
            'work' => $work,
            'filters' => $filters,
        ]);
    }
    public function update(Request $request, $id){
        $this->validate($request,[
            'title' => 'required',
            'content' => 'required',
            'date' => 'required',
            'links' => 'required',
            'image' => 'nullable|image',
            'image_two' => 'nullable|image',
            'image_three' => 'nullable|image',
            'image_blocks' => 'nullable|image',
        ]);
        $work = Work::find($id);
        $work->edit($request->all());
        $work->uploadImage($request->file('image'));
        $work->uploadImageTwo($request->file('image_two'));
        $work->uploadImageThree($request->file('image_three'));
        $work->uploadImageBlocks($request->file('image_blocks'));
        $work->toggleView($request->get('is_slider'));
        $work->toggleImgBlocks($request->get('blocks_image'));
        return redirect()->route('works_up.index')->with('message','Проект успешно обновлен');
    }
    public function destroy($id){
        Work::find($id)->remove();
        return redirect()->route('works_up.index')->with('message','Проект успешно удален');
    }
}
