<?php

namespace App\Http\Controllers\Backend;

use App\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class TagsController extends Controller
{
    public function index(){
        $tags = DB::table('tags')->orderBy('created_at','desc')->paginate(7);
        return view('backend.tags.index', [
            'tags' => $tags
            ]
        );
    }
    public function create() {
        return view('backend.tags.create');
    }
    public function store(Request $request) {
        $this->validate($request,[
           'title' => 'required'
        ]);
        Tag::create($request->all());
        return redirect()->route('tags.index')->with('message','Тег успешно добавлен');
    }
    public function edit($id) {
        $tag = Tag::find($id);
        return view('backend.tags.edit',[
            'tag' => $tag
        ]);
    }
    public function update(Request $request, $id){
        $this->validate($request,[
            'title' => 'required'
        ]);
        $tag = Tag::find($id);
        $tag->update($request->all());
        return redirect()->route('tags.index')->with('message','Тег успешно обновлен');
    }
    public function destroy($id){
        Tag::find($id)->delete();
        return redirect()->route('tags.index')->with('message','Тег успешно удален');
    }

}
