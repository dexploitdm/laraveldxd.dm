<?php

namespace App\Http\Controllers\Backend;

use App\Category;
use App\Menu;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenusController extends Controller
{
    public function index() {
        $menus = $this->getMenus();
        return view('backend.menus.index',[
            'menus' => $menus
        ]);
    }
    public function getMenus() {
        $menu = Menu::get();
        return $menu;
//        if($menu->isEmpty()) {
//            return FALSE;
//        }
//
//        return Menu::make('forMenuPart', function($m) use($menu) {
//            foreach($menu as $item) {
//                if($item->parent == 0) {
//                    $m->add($item->title, $item->path)->id($item->id);
//                }
//                else {
//                    if($m->find($item->parent)) {
//                        $m->find($item->parent)->add($item->title, $item->path)->id($item->id);
//                    }
//                }
//            }
//        });
    }

    public function create()
    {
        $tmp = $this->getMenus();
        $menus = $tmp->reduce(function($returnMenus, $menu) {
            $returnMenus[$menu->id] = $menu->title;
            return $returnMenus;
        },['0' => 'Родительский пункт навигации']);
        $categories = Category::select('*')->get();
        $list = array();
        $list = array_add($list,'0','Не используется');
        $list = array_add($list,'parent','Раздел статьи');
        foreach ($categories as $category) {
            if($category->parent_id == 0) {
                $list[$category->title] = array();
            }
            else {
                $list[$categories->where('id',$category->parent_id)->first()->title][$category->slug] = $category->title;
            }
        }
        $blogs = Post::get(['id','slug','title'])->reduce(function ($returnBlogs, $blog) {
            $returnBlogs[$blog->slug] = $blog->title;
            return $returnBlogs;
        }, []);
       return view('backend.menus.create',[
            'menus'=>$menus,
            'categories'=>$list,
            'blogs' => $blogs
        ]);

    }

    public function store(Request $request) {
        $result = Menu::addMenu($request);
        if(is_array($result) && !empty($result['error'])) {
            return back()->with($result);
        }
        return redirect()->route('menus.index')->with('message','Ссылка меню успешно добавлена');
    }
    public function edit($id)
    {   $menu = Menu::find($id);
        $type = FALSE; //Какой тип меню редактируем
        $option = FALSE; //Активность радио кнопки
        $route = (app('router')->getRoutes()->match(app('request')->create($menu->path)));
        $aliasRoute = $route->getName();
        $parameters = $route->parameters();// Параметры маршрута
        if($aliasRoute == 'blogs.index' || $aliasRoute == 'blogCat') {
            $type = 'blogLink';
            $option = isset($parameters['cat_alias']) ? $parameters['cat_alias'] : 'parent';
        }
        else if($aliasRoute == 'blogs.show') {
            $type = 'blogLink';
            $option = isset($parameters['alias']) ? $parameters['alias'] : '';
        }

        else {
            $type = 'customLink';
        }

        $tmp = $this->getMenus();
        $menus = $tmp->reduce(function($returnMenus, $menu) {
            $returnMenus[$menu->id] = $menu->title;
            return $returnMenus;
        },['0' => 'Родительский пункт навигации']);
        $categories = Category::select(['title','slug','parent_id','id'])->get();
        $list = array();
        $list = array_add($list,'0','Не используется');
        $list = array_add($list,'parent','Раздел блог');
        foreach ($categories as $category) {
            if($category->parent_id == 0) {
                $list[$category->title] = array();
            }
            else {
                $list[$categories->where('id',$category->parent_id)->first()->title][$category->alias] = $category->title;
            }
        }

        $blogs = Post::get(['id','slug','title'])->reduce(function ($returnBlogs, $blog) {
            $returnBlogs[$blog->slug] = $blog->title;
            return $returnBlogs;
        }, []);

        return view('backend.menus.edit')->with([
            'menus'=>$menus,
            'menu' => $menu,
            'type'=>$type,
            'option'=>$option,
            'categories'=>$list,
            'blogs' => $blogs
        ]);
    }



    public function update(Request $request, $id)
    {
        $data = $request->only('type','title','parent');
        if(empty($data)) {
            return ['error' => 'Нет данных'];
        }

        switch($data['type']) {
            case 'customLink':
                $data['path'] = $request->input('custom_link');
                break;
            case 'blogLink':
                if($request->input('category_alias')) {
                    if($request->input('category_alias') == 'parent') {
                        $data['path'] = route('blog.index');
                    }
                    else {
                        $data['path'] = route('category.show',['cat_alias' => $request->input('category_alias')]);
                    }
                }
                else if($request->input('blog_alias')) {
                    $data['path'] = route('blog.show',['slug' =>$request->input('blog_alias')]);
                }
                break;
        }

        unset($data['type']);
        $menu = Menu::find($id);

        $menu->update($request->all());
        dump($data);
        return redirect()->route('menus.index')->with('message','Ссылка меню успешно обновлена');

    }


    public function destroy($id){
        $menu =Menu::where('id',$id)->first();
        $menu->delete();
        return redirect()->route('menus.index')->with('message','Ссылка успешно удалена');
    }


}
