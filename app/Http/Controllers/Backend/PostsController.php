<?php

namespace App\Http\Controllers\Backend;

use App\Category;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    public function index() {
        $posts = Post::orderBy('created_at','desc')->paginate(7);
        return view('backend.posts.index', ['posts' => $posts]);
    }
    public function create(){
        $tags = Tag::pluck('title','id')->all();
        $categories = Category::pluck('title','id')->all();
        return view('backend.posts.create',[
            'tags' => $tags,
            'categories' => $categories
        ]);
    }
    public function store(Request $request){
        $this->validate($request,[
            'title' => 'required',
            'content' => 'required',
            'image' => 'nullable|image'
        ]);
        $post = Post::add($request->all());
        $post->uploadImage($request->file('image'));
        $post->setCategory($request->get('category_id'));
        $post->setTags($request->get('tags'));
        $post->toggleStatus($request->get('status'));
        $post->toggleFeatured($request->get('is_featured'));
        return redirect()->route('posts.index')->with('message','Статья успешно добавлена');
    }
    public function edit($id){
        $post = Post::find($id);
        $tags = Tag::pluck('title','id')->all();
        $categories = Category::pluck('title','id')->all();
        $selectedTags = $post->tags->pluck('id')->all();
        return view('backend.posts.edit',[
            'post' => $post,
            'tags' => $tags,
            'categories' => $categories,
            'selectedTags' => $selectedTags
        ]);
    }
    public function update(Request $request, $id){
        $this->validate($request,[
            'title' => 'required',
            'content' => 'required',
            'date' => 'required',
            'image' => 'nullable|image'
        ]);
        $post = Post::find($id);
        $post->edit($request->all());
        $post->uploadImage($request->file('image'));
        $post->setCategory($request->get('category_id'));
        $post->setTags($request->get('tags'));
        $post->toggleStatus($request->get('status'));
        $post->toggleFeatured($request->get('is_featured'));
        return redirect()->route('posts.index')->with('message','Статья успешно обновлена');
    }
    public function destroy($id){
        Post::find($id)->remove();
        return redirect()->route('posts.index')->with('message','Статья успешно удалена');
    }

}
