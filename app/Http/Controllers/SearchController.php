<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class SearchController extends Controller
{
    public function index(Post $model, Request $request)
    {
        $name = Input::get('q');
        $searchResult = Post::where('title', 'LIKE', "%$name%")->get();
        return View('frontend.search.index')
            ->with('name', $name)
            ->with('searchResult', $searchResult);
    }

}
