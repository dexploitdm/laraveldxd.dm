<?php

namespace App\Http\Controllers\Frontend;

use App\Subscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\SubscribeEmail;

class SubsController extends Controller
{
    public function subscribe(Request $request)
    {
        $this->validate($request, [
            'email'	=>	'required|email|unique:subscriptions'
        ]);

        $subs = Subscription::add($request->get('email'));
        $subs->generateToken();

        \Mail::to($subs)->send(new SubscribeEmail($subs));

        return redirect()->back()->with('status','Для активации проверьте вашу почту!');
    }

    public function verify($token)
    {
        $subs = Subscription::where('token', $token)->firstOrFail();
        $subs->token = null;
        $subs->save();
        return redirect('/')->with('status', 'Ваша почта подтверждена! СПАСИБО!');
    }
}
