<?php

namespace App\Http\Controllers\Frontend;

use App\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServicesController extends Controller
{
    public function index() {
        $services = Service::all();
        return view('frontend.services.index',[
            'services' => $services
        ]);
    }

    public function show($slug){
        $service = Service::where('slug', $slug)->firstOrFail();
        return view('frontend.services.show',[
            'service' => $service,
        ]);
    }
}
