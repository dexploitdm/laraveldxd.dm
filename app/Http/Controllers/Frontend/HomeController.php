<?php

namespace App\Http\Controllers\Frontend;

use App\Framework;
use App\Menu;
use App\Service;
use App\Slider;
use App\Work;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class HomeController extends Controller
{
    public function index() {
        $count = 0;
        $services = Service::all();
        $works = Work::all();
        $frames = Framework::all();
        $sliders = Slider::all();
        return view('frontend.index',[
            'sliders' => $sliders,
            'count' => $count,
            'services' => $services,
            'works' => $works,
            'frames' => $frames
        ]);
    }
}
