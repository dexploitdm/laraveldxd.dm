<?php

namespace App\Http\Controllers\Frontend;

use App\Mail\SendEmail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MailController extends Controller
{
    public function order(Request $request)
    {
        $this->validate($request, [
            'email'	=>	'required|email|unique:subscriptions'
        ]);

        $order = new \stdClass();
        $order->viewsite = $request->get('viewsite');
        $order->name = $request->get('name');
        $order->email =$request->get('email');
        $order->phone = $request->get('phone');
        $order->company = $request->get('company');
        $order->message = $request->get('message');
        \Mail::to($order)->send(new SendEmail($order));
        return redirect()->back()->with('status','Ваша заявка отправлена!');
    }
}
