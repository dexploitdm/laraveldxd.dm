<?php

namespace App\Http\Controllers\Frontend;

use App\Filter;
use App\Work;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class WorksController extends Controller
{
    public function index() {
        $filters = Cache::get('cacheIdFilter', function () {$data = Filter::all();
            Cache::put('cacheIdFilter', $data, 15);
            return $data;});
//        $works = Cache::get('cacheId', function () {$data = Work::all();
//            Cache::put('cacheId', $data, 15);
//            return $data;});
        $works = Work::all();
        return view('frontend.works.index',[
            'works' => $works,
            'filters' => $filters
        ]);
    }
    public function show($slug){
        $work = Work::where('slug', $slug)->firstOrFail();
        return view('frontend.works.show',[
            'work' => $work,
        ]);
    }
    public function like($id) {
        $work = Work::find($id);
        $likes = $work->likes;
        $likes = $likes +1;
        $work->likes = $likes;
        $work->save();
        return redirect('works/'.$work->slug .'#likes');

    }

}
