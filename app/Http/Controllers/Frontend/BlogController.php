<?php

namespace App\Http\Controllers\Frontend;

use App\Category;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    public function index() {
        $posts = Post::where('status',1)->paginate(7);
        return view('frontend.blogs.index',[
            'posts' => $posts
        ]);
    }

    public function show($slug){
        //Проверяем если slug у двнной записи
        $post = Post::where('slug', $slug)->firstOrFail();

        $previous = Post::where('id', '<', $post->id)->max('id');


        return view('frontend.blogs.show',[
            'post' => $post,
            'previous' => $previous
        ]);
    }

    public function tag($slug){
        $tag = Tag::where('slug', $slug)->firstOrFail();
        //Берем статьи от текущего тега
        //$posts = $tag->posts()->where('status', 1)->paginate(2);
        $posts = $tag->posts()->paginate(2);
        return view('frontend.blogs.lists_tags', ['posts' => $posts, 'tag' => $tag]);
    }

    public function category($slug){
        $category = Category::where('slug', $slug)->firstOrFail();
        //Берем статьи от текущего тега
        $posts = $category->posts()->paginate(2);
        return view('frontend.blogs.lists_cats', ['posts' => $posts,'category' => $category]);
    }

}
