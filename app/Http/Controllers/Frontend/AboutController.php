<?php

namespace App\Http\Controllers\Frontend;

use App\About;
use App\AboutSlider;
use App\Commands;
use App\Framework;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    public function index(){
        $frameworks = Framework::all();
        $commands = Commands::all();
        $absliders = AboutSlider::all();
        $abouts_one = About::orderBy('created_at','asc')->paginate(1);
        $abouts_two = About::orderBy('created_at','desc')->paginate(1);
        return view('frontend.about.index',[
            'abouts_one' => $abouts_one,
            'abouts_two' => $abouts_two,
            'absliders' => $absliders,
            'commands' => $commands,
            'frameworks' => $frameworks
        ]);
    }
}
