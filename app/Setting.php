<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Setting extends Model
{
    protected $fillable = ['title','address','phone','time_work','logo','banner','facebook','vk',
        'twitter','google','youtube','github','banner_text','banner_link','banner_title'];

    //Добавление
    public static function add($fields){
        $setting = new static();
        $setting->fill($fields);
        $setting->save();
        return $setting;
    }
    //Редактирование
    public function edit($fields) {
        $this->fill($fields);
        $this->save();
    }
    //Удаление
    public function remove() {
        $this->removeLogo();
        $this->removeBanner();
        $this->delete();
    }
    public function removeLogo(){
        if($this->logo != null){
            Storage::delete('uploads/other' . $this->logo);
        }
    }
    public function removeBanner(){
        if($this->banner != null){
            Storage::delete('uploads/other' . $this->banner);
        }
    }
    //Изображение
    public function uploadImage($logo) {
        if($logo == null){ return; }
        $this->removeLogo();
        $filename =str_random(10) . '.' . $logo->extension();
        $logo->storeAs('uploads/other',$filename);
        $this->logo = $filename;
        $this->save();
    }
    //Изображение
    public function uploadBanner($banner) {
        if($banner == null){ return; }
        $this->removeBanner();
        $filename =str_random(10) . '.' . $banner->extension();
        $banner->storeAs('uploads/other',$filename);
        $this->banner = $filename;
        $this->save();
    }
    public function getLogo() {
        if($this->logo == null) {
            return 'img/no-image.png';
        }
        return '/uploads/other/' . $this->logo;
    }
    public function getBanner() {
        if($this->banner == null) {
            return 'img/no-image.png';
        }
        return '/uploads/other/' . $this->banner;
    }
}
