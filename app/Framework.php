<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Framework extends Model
{
    protected $fillable = ['title','image'];

    //Добавление
    public static function add($fields){
        $framework = new static();
        $framework->fill($fields);
        $framework->save();
        return $framework;
    }
    //Редактирование
    public function edit($fields) {
        $this->fill($fields);
        $this->save();
    }
    //Удаление
    public function remove() {
        $this->removeImage();
        $this->delete();
    }
    public function removeImage(){
        if($this->image != null){
            Storage::delete('uploads/other' . $this->image);
        }
    }
    //Изображение
    public function uploadImage($image) {
        if($image == null){ return; }
        $this->removeImage();
        $filename =str_random(10) . '.' . $image->extension();
        $image->storeAs('uploads/other',$filename);
        $this->image = $filename;
        $this->save();
    }
    public function getImage() {
        if($this->image == null) {
            return 'img/no-image.png';
        }
        return '/uploads/other/' . $this->image;
    }
}
