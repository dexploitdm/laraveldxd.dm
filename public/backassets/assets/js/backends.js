//open dropzone
jQuery( document ).ready( function() {
    $(".customdropzone").click(function () {
        $('#imageInput').click();
        $('.customdropzone_title').hide();
        $("#removePreview").addClass('visiblebtn');
    });
} );

//Dropzone custom
var input = document.getElementById("imageInput")
var btn = document.getElementById("removePreview")
var output = document.getElementById("preview")

input.addEventListener('change', this.handlePreview)
btn.addEventListener('click', this.removeImage)

function handlePreview(f) {
    removeImage(f)
    file = f.target.files[0] || f.dataTransfer.files[0]
    var image = new Image()
    var reader = new FileReader()
    var imgElem = document.createElement('img')

    reader.onload = function (e) {
        imgElem.src = e.target.result
        output.appendChild(imgElem)
    }
    reader.readAsDataURL(file)
}
function removeImage(e) {
    if (e.type === 'click') {
        input.value = ''
    }
    while(output.firstChild) {
        output.removeChild(output.firstChild)
    }
}
//Замена текста у label при checkbox
$(document).ready(function(){
    //Замена текста label при checkbox Статус блокировки
    $(function(){
        $('#someSwitchOptionSuccess').on('change', function(){
            if($('#someSwitchOptionSuccess').prop('checked')){
                $("#labelban").text("Разблокировать");
            }else{
                $("#labelban").text("Заблокировать");
            }
        });
    });
    $(function(){
        $('#someSwitchOptionDefault').on('change', function(){
            if($('#someSwitchOptionDefault').prop('checked')){
                $("#labeladmin").text("Админ");
            }else{
                $("#labeladmin").text("Пользователь");
            }
        });
    });
});
//Изображение 2
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imgInput").change(function(){
    readURL(this);
});
//Изображение 3
function readURLTh(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image3').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imgInput3").change(function(){
    readURLTh(this);
});
//Изображение блоков
function readURLBlocks(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#imageBlocks').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imgInputBlocks").change(function(){
    readURLBlocks(this);
});